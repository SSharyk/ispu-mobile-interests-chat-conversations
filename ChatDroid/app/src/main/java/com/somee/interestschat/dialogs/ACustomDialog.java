package com.somee.interestschat.dialogs;

import android.app.Dialog;
import android.content.Context;

/**
 * Created by Alexander on 08.10.2016.
 */
abstract class ACustomDialog extends Dialog {
    protected Context context;

    /**
     * General constructor for the all custom dialogs
     * @param context Context of the dialog
     */
    public ACustomDialog(Context context) {
        super(context);
        this.context = context;
        this.setDialogContent();
        this.setDialogControlButtons();
    }

    /**
     * Sets the main content of the dialog
     */
    protected abstract void setDialogContent();

    /**
     * Sets the control buttons of the dialog
     */
    protected abstract void setDialogControlButtons();
}
