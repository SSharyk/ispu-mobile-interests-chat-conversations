package com.somee.interestschat.dialogs;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.somee.interestschat.R;
import com.somee.interestschat.activities.FriendsActivity;
import com.somee.interestschat.adapters.UsersAdapter;
import com.somee.interestschat.enums.GroupPrivacy;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;
import com.somee.interestschat.models.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alexander on 08.10.2016.
 */
public class UserSelectionDialog extends ACustomSelectionDialog<User> {

    private UsersAdapter adapter;

    /**
     * General constructor
     * @param context Context of the dialog
     */
    public UserSelectionDialog(Context context) {
        super(context);
    }

    /**
     * Sets the main content of the dialog
     */
    @Override
    protected void setDialogContent() {
        this.setContentView(R.layout.select_user_dialog);
        this.setTitle(R.string.dialog_find_friends_title);

        this.setListView(null);
        this.allEntities = entitiesList;
        this.setPaginationButtons();
    }

    /**
     * Sets the list of entities as a content
     * @param users All required entities
     */
    @Override
    protected void setListView(ArrayList<User> users) {
        if (users == null) {
            TextView pageTextField = (TextView) this.findViewById(R.id.txtPageNumber);
            pageTextField.setText(String.valueOf(pageNumber + 1));

            users = new ArrayList<>();
            try {
                String path = Global.ServerConnectionString;
                path += getGettingUsersPath();
                String response = new BasicDataService().execute(path).get();
                JSONArray array = new JSONArray(response);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject us_json = array.getJSONObject(i);
                    User user = new User(us_json);
                    users.add(user);
                }

                int currentUserIndex = -1;
                for (int i = 0; i < users.size(); i++) {
                    if (users.get(i).getId() == Global.CurrentUser) {
                        currentUserIndex = i;
                        break;
                    }
                }
                if (currentUserIndex != -1)
                    users.remove(currentUserIndex);
            } catch (Exception e) {
                Log.e("mErr", e.getMessage());
            }
        }

        entitiesList = users;
        this.adapter = new UsersAdapter((Activity) context, true, this, getEntitiesInCurrentPage());
        this.entitiesListView = (ListView) this.findViewById(R.id.lstAllUsersForSelect);

        entitiesListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_multiple_choice, getUsersNames());
        entitiesListView.setAdapter(adapter);

        entitiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                User selected = entitiesList.get(i);
                checkClick(selected);
            }
        });
    }

    /**
     * Maps users to their logins
     * @return List of users' names
     */
    private ArrayList<CharSequence> getUsersNames() {
        ArrayList<CharSequence> list = new ArrayList<>();
        for (User user : entitiesList) {
            list.add(user.getLogin());
        }
        return list;
    }


    /**
     * Sets the control buttons of the dialog
     */
    @Override
    protected void setDialogControlButtons() {
        okButton = (Button) this.findViewById(R.id.cmdSelectUserDialogOk);
        okButton.setText("OK : " + TotalSelected);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    for (User user : selectedEntities) {
                        try {
                            String path = Global.ServerConnectionString;
                            boolean isForFriends = ((Activity) context) instanceof FriendsActivity;
                            path += (isForFriends)
                                    ? "/invite/friendship/" + Global.CurrentUser + "/" + user.getId()
                                    : "/invite/membership/" + Global.CurrentUser + "/" + user.getId() + "/" + Global.CurrentConversation.getId();
                            new BasicDataService().execute(path).get();
                        } catch (Exception e) {
                        }
                    }
                } catch (Exception e) {
                }
                cancel();
            }
        });

        cancelButton = (Button) this.findViewById(R.id.cmdSelectUserDialogCancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    /**
     * Constructs the URI in according to aims
     * @return URI
     */
    private String getGettingUsersPath() {
        boolean isForFriends = ((Activity) context) instanceof FriendsActivity;
        String path = "";
        if (isForFriends) {
            path = "/user/allusersnotfriends/" + Global.CurrentUser;
        } else {
            path = (Global.CurrentConversation.getPrivacy() == GroupPrivacy.Закрытая)
                    ? "/user/friends/" + Global.CurrentUser
                    : "/user/allusers";
        }
        return path;
    }
}