package com.somee.interestschat.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.somee.interestschat.R;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.models.Message;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Alexander on 03.10.2016.
 */
public class MessagesAdapter extends BaseAdapter {
    private Activity context;
    private ArrayList<Message> messagesList;

    public MessagesAdapter(Activity context, ArrayList<Message> messagesList) {
        this.context = context;
        this.messagesList = messagesList;
    }

    @Override
    public int getCount() {
        return messagesList.size();
    }

    @Override
    public Object getItem(int i) {
        return messagesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        Message item = messagesList.get(i);

        LayoutInflater layoutInflater = context.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.message_item, null);

        TextView messageText = (TextView) view.findViewById(R.id.txtMessageText);
        messageText.setText(item.getMessageText());

        TextView userName = (TextView) view.findViewById(R.id.txtUserName);
        userName.setText(item.getUser().getLogin());

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        TextView messageDate = (TextView) view.findViewById(R.id.txtMessageDate);
        messageDate.setText(dateFormat.format(item.getPostDateTime()));

        ImageView userAvatar = (ImageView) view.findViewById(R.id.imgUserAvatar);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) userAvatar.getLayoutParams();
        if (item.getUserId()== Global.CurrentUser){
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }
        else{
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        }

        return view;
    }
}
