package com.somee.interestschat.models;

import com.somee.interestschat.enums.MimeType;
import com.somee.interestschat.fakes.Global;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Alexander on 05.10.2016.
 */
public class BoardItem implements IFilterable {
    // region Id
    protected int id;

    public int getId() {
        return id;
    }

    protected void setId(int id) {
        this.id = id;
    }
    // endregion

    // region Title
    protected String title;

    public String getTitle() {
        return title;
    }

    protected void setTitle(String title) {
        this.title = title;
    }
    // endregion

    // region MIME
    protected MimeType mimeType;

    public MimeType getMimeType() {
        return mimeType;
    }

    protected void setMimeType(MimeType mimeType) {
        this.mimeType = mimeType;
    }
    //endregion

    // region UserId
    protected int userId;

    public int getUserId() {
        return userId;
    }

    protected void setUserId(int userId) {
        this.userId = userId;
    }
    // endregion

    // region ConversationId
    protected int conversationId;

    public int getConversationId() {
        return conversationId;
    }

    protected void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }
    // endregion

    // region LastUpdateDateTime
    protected Date lastUpdateDateTime;

    public Date getLastUpdateDateTime() {
        return lastUpdateDateTime;
    }

    protected void setLastUpdateDateTime(Date lastUpdateDateTime) {
        this.lastUpdateDateTime = lastUpdateDateTime;
    }
    // endregion

    // region Constructors
    public BoardItem(JSONObject mes) throws JSONException, ParseException {
        setId(mes.getInt("Id"));
        setTitle(mes.getString("Title"));
        setMimeType(MimeType.getById(mes.getInt("MimeId")));
        setConversationId(mes.getInt("ConversationId"));
        setUserId(mes.getInt("UserId"));
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        setLastUpdateDateTime(format.parse(mes.getString("LastUpdateDateTime")));
    }

    public BoardItem(){
        setId(-1);
        setTitle("");
        setMimeType(MimeType.Note);
        setConversationId(Global.CurrentConversation.getId());
        setUserId(Global.CurrentUser);
        setLastUpdateDateTime(new Date());
    }
    // endregion

    @Override
    public boolean isSatisfy(CharSequence filterString) {
        return this.getTitle().toLowerCase().contains(filterString.toString().toLowerCase());
    }
}