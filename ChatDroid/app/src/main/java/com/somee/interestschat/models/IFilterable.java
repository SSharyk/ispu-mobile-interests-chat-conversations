package com.somee.interestschat.models;

/**
 * Created by Alexander on 11.11.2016.
 */
public interface IFilterable {
    boolean isSatisfy(CharSequence filterString);
}
