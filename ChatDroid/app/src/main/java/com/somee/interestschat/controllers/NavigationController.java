package com.somee.interestschat.controllers;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import com.somee.interestschat.activities.FriendsActivity;
import com.somee.interestschat.activities.GroupsActivity;
import com.somee.interestschat.R;
import com.somee.interestschat.activities.InvitesActivity;
import com.somee.interestschat.activities.RegistrationActivity;
import com.somee.interestschat.enums.NavigationItems;
import com.somee.interestschat.fakes.Global;

/**
 * Created by Alexander on 08.10.2016.
 */

/**
 * Represents navigation menu
 */
public class NavigationController {
    private AppCompatActivity context;
    private String viewName;
    private MyActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ListView mLeftDrawer;
    //private TextView mTitle;
    private ImageButton menuBtn;
    private ArrayAdapter mLeftAdapter;
    private String[] mLeftDataSet;

    /**
     * Creates new controller
     * @param context Activity as a context
     * @param viewName View name for representing
     */
    public NavigationController(AppCompatActivity context, String viewName) {
        this.context = context;
        this.viewName = viewName;
        mLeftDataSet = NavigationItems.getValuesAsStringArray();
        GetViewItems();
        SetView();
        SubscribeView();
    }

    /**
     * Getting view items
     */
    private void GetViewItems() {
        mDrawerLayout = (DrawerLayout) context.findViewById(R.id.drawer_layout);
        mLeftDrawer = (ListView) context.findViewById (R.id.left_drawer);
        menuBtn  =(ImageButton) context.findViewById(R.id.toolbarMenuButton);
    }

    /**
     * Setting listeners for nav menu items
     */
    private void SubscribeView() {
        mLeftDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        OnFriendsClick();
                        break;
                    case 1:
                        OnGroupsClick();
                        break;
                    case 2:
                        OnInvitesClick();
                        break;
                    case 3:
                        OnLogoutClick();
                        break;
                }

                //SupportFragmentManager.BeginTransaction().Replace(Resource.Id.main, fragment).Commit();
                mDrawerLayout.closeDrawers();
                mDrawerToggle.syncState();
            }
        });

        menuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(mLeftDrawer)) {
                    mDrawerLayout.closeDrawer(mLeftDrawer);
                } else {
                    mDrawerLayout.openDrawer(mLeftDrawer);
                }
            }
        });
    }

    /**
     * Sets view items
     */
    private void SetView() {
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        context.getSupportActionBar().setHomeButtonEnabled(true);
        context.getSupportActionBar().setDisplayShowTitleEnabled(true);
        context.getSupportActionBar().setDisplayShowCustomEnabled(true);
        mDrawerToggle = new MyActionBarDrawerToggle(
                context, //Host Activity
                mDrawerLayout, //DrawerLayout
                R.string.openDrawer, //Opened Message
                R.string.main_menu_activity_title, //Closed Message
                viewName
        );
        mDrawerToggle.syncState();
        context.getSupportActionBar().setTitle(R.string.openDrawer);
        //mTitle.Text = viewName;
        mLeftDrawer.setTag(0);
        ArrayAdapter<String> ListAdapter =
                new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, mLeftDataSet);
        mLeftDrawer.setAdapter(ListAdapter);
    }

    private void OnFriendsClick() {
        Intent intent = new Intent(context, FriendsActivity.class);
        context.startActivity(intent);
    }

    private void OnGroupsClick() {
        Intent intent = new Intent(context, GroupsActivity.class);
        context.startActivity(intent);
    }

    private void OnInvitesClick() {
        Intent intent = new Intent(context, InvitesActivity.class);
        context.startActivity(intent);
    }

    private void OnLogoutClick() {
        Global.CurrentUser = -1;
        Intent intent = new Intent(context, RegistrationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }
}