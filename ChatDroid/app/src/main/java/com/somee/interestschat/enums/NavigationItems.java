package com.somee.interestschat.enums;

/**
 * Created by Alexander on 08.10.2016.
 */
public enum NavigationItems {
    Друзья,
    Беседы,
    Приглашения,
    Выход;

    public static String[] getValuesAsStringArray(){
        NavigationItems[] items = NavigationItems.values();
        String[] vals = new String[items.length];
        for(int i=0; i<items.length; i++){
            vals[i] = items[i].toString();
        }
        return vals;
    }
}
