package com.somee.interestschat.fakes;

import com.somee.interestschat.models.Conversation;

/**
 * Created by Alexander on 31.10.2016.
 */
public class Global {
    public static int CurrentUser;
    private static boolean isAuth;

    public static boolean isAuthorized() {
        return isAuth;
    }

    public static void isAuthorized(boolean isAuth) {
        Global.isAuth = isAuth;
    }


    public static Conversation CurrentConversation;

    public static final String ServerConnectionString = "http://chatapi.somee.com/api";
}