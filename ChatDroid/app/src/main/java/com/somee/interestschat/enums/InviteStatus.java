package com.somee.interestschat.enums;

/**
 * Created by Alexander on 05.10.2016.
 */
public enum InviteStatus {
    Send,
    Read,
    Accepted,
    Canceled
}
