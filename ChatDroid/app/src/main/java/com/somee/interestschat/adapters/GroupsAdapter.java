package com.somee.interestschat.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.somee.interestschat.R;
import com.somee.interestschat.enums.GroupPrivacy;
import com.somee.interestschat.models.Conversation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Alexander on 03.10.2016.
 */
public class GroupsAdapter extends BaseAdapter {

    private HashMap<String, ArrayList<Conversation>> groupsItems;
    private ListView groupsList;
    private Activity context;

    public GroupsAdapter(Activity context, HashMap<String, ArrayList<Conversation>> groupsItems)
    {
        this.context = context;
        this.groupsItems = groupsItems;
    }

    @Override
    public int getCount() {
        int count = 0;
        for (ArrayList<Conversation> group : groupsItems.values())
            count += group.size() + 1;
        return count;
    }

    @Override
    public Object getItem(int i) {
        for (ArrayList<Conversation> group : groupsItems.values()) {
            int size = group.size() + 1;
            if (i == 0)
                return null;
            if (i < size)
                return group.get(i - 1);
            i -= size;
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        int type = GetItemViewType(position);
        View view = convertView;
        if (type == 0)
        {
            view = context.getLayoutInflater().inflate(R.layout.group_section_header, null);
            setMenuSectionHeader(view, position);
        }
        else
        {
            view = context.getLayoutInflater().inflate(R.layout.group_item, null);
            setMenuItem(view, position);
        }
        return view;
    }

    public int GetItemViewType(int position) {
        for (ArrayList<Conversation> group : groupsItems.values()) {
            int size = group.size() + 1;
            if (position == 0)
                return 0;
            if (position < size)
                return 1;
            position -= size;
        }
        throw new NullPointerException();
    }

    protected String GetGroupName(int position)
    {
        for (String key : groupsItems.keySet())
        {
            ArrayList<Conversation> group = groupsItems.get(key);
            int size = group.size() + 1;
            if (position < size)
                return key;
            position -= size;
        }
        throw new NullPointerException();
    }

    protected void setMenuSectionHeader(View view, int position)
    {
        TextView MenuSectionHeaderText = (TextView) view.findViewById(R.id.MenuSectionHeaderName);
        MenuSectionHeaderText.setText(GetGroupName(position).toUpperCase());
    }

    protected void setMenuItem(View view, int position) {
        Conversation item = (Conversation) this.getItem(position);

        ImageView ConversationPrivacyLogo = (ImageView) view.findViewById(R.id.ConversationItemPrivacyLogo);
        ConversationPrivacyLogo.setImageResource((item.getPrivacy() == GroupPrivacy.Закрытая)
                ? R.drawable.conv_private
                : R.drawable.conv_public);

        TextView ConversationItemName = (TextView) view.findViewById(R.id.ConversationItemName);
        ConversationItemName.setText(item.getName());

        TextView ConversationItemDescription = (TextView) view.findViewById(R.id.ConversationItemDescription);
        ConversationItemDescription.setText(item.getLastMessageText());
    }
}
