package com.somee.interestschat.dialogs;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.somee.interestschat.R;
import com.somee.interestschat.fakes.GroupFakes;

/**
 * Created by Alexander on 08.10.2016.
 */
public class ConversationCreatingDialog extends ACustomDialog {

    /**
     * General constructor
     * @param context Context of the dialog
     */
    public ConversationCreatingDialog(Context context) {
        super(context);
    }

    /**
     * Sets the main content of the dialog
     */
    @Override
    protected void setDialogContent() {
        this.setContentView(R.layout.conversation_create_new_dialog);
        this.setTitle(R.string.dialog_create_conversation_title);

        final Spinner privacySpinner = (Spinner) this.findViewById(R.id.comboPrivacy);
        ArrayAdapter<CharSequence> privacyAdapter = ArrayAdapter.createFromResource(context,
                R.array.privacies_array, android.R.layout.simple_spinner_item);
        privacyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        privacySpinner.setAdapter(privacyAdapter);

        final Spinner categorySpinner = (Spinner) this.findViewById(R.id.comboCategory);
        ArrayAdapter<CharSequence> categoryAdapter = new ArrayAdapter<CharSequence>(context,
                android.R.layout.simple_spinner_item, GroupFakes.getCategories());
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoryAdapter);
    }

    @Override
    protected void setDialogControlButtons() {
    }
}