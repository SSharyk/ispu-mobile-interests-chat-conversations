package com.somee.interestschat.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.somee.interestschat.R;
import com.somee.interestschat.activities.GroupsActivity;
import com.somee.interestschat.adapters.ConversationsAdapter;
import com.somee.interestschat.enums.GroupPrivacy;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;
import com.somee.interestschat.models.Conversation;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alexander on 30.10.2016.
 */
public class ConversationSelectionDialog extends ACustomSelectionDialog<Conversation> {
    private ConversationsAdapter adapter;

    /**
     * General constructor
     * @param context Context of the dialog
     */
    public ConversationSelectionDialog(Context context) {
        super(context);
    }

    /**
     * Sets the main content of the dialog
     */
    @Override
    protected void setDialogContent() {
        this.setContentView(R.layout.select_conversation_dialog);
        this.setTitle(R.string.dialog_find_conversations_title);

        this.setListView(null);
        this.allEntities = new ArrayList<>(entitiesList);
        this.setPaginationButtons();
    }

    /**
     * Sets the list of entities as a content
     * @param entities All required entities
     */
    @Override
    protected void setListView(ArrayList<Conversation> entities) {
        if (entities == null) {
            TextView pageTextField = (TextView) this.findViewById(R.id.txtPageNumber);
            pageTextField.setText(String.valueOf(pageNumber + 1));

            entities = new ArrayList<>();
            try {
                String path = Global.ServerConnectionString + "/conversation/publicForSelect/" + Global.CurrentUser;
                String response = new BasicDataService().execute(path).get();
                JSONArray array = new JSONArray(response);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject conv_json = array.getJSONObject(i);
                    Conversation conversation = new Conversation(conv_json);
                    if (conversation.getPrivacy() == GroupPrivacy.Открытая)
                        entities.add(conversation);
                }
            } catch (Exception e) {
            }
        }

        entitiesList = entities;
        this.adapter = new ConversationsAdapter((Activity) context, this, getEntitiesInCurrentPage());
        this.entitiesListView = (ListView) this.findViewById(R.id.lstAllPublicConversationsForSelect);
        entitiesListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        final ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_multiple_choice, getConversationTitles());
        entitiesListView.setAdapter(adapter);

        entitiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Conversation selected = entitiesList.get(i);
                checkClick(selected);
            }
        });
    }

    /**
     * Maps conversations to their titles
     * @return List of conversations' titles
     */
    private ArrayList<CharSequence> getConversationTitles() {
        ArrayList<CharSequence> list = new ArrayList<>();
        for (Conversation conversation : entitiesList) {
            list.add(conversation.getName());
        }
        return list;
    }


    /**
     * Sets the control buttons of the dialog
     */
    @Override
    protected void setDialogControlButtons() {
        okButton = (Button) this.findViewById(R.id.cmdSelectConversationDialogOk);
        okButton.setText("OK : " + TotalSelected);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    for (Conversation conversation : selectedEntities) {
                        try {
                            String path = Global.ServerConnectionString + "/conversation/add/" + Global.CurrentUser + "/" + conversation.getId();
                            new BasicDataService().execute(path).get();
                            ((GroupsActivity) context).updateList();
                        } catch (Exception e) {
                        }
                    }
                } catch (Exception e) {
                }
                cancel();
                setListView(null);
            }
        });

        cancelButton = (Button) this.findViewById(R.id.cmdSelectConversationDialogCancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }
}