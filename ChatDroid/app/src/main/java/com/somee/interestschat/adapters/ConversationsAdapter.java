package com.somee.interestschat.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.somee.interestschat.R;
import com.somee.interestschat.dialogs.ConversationSelectionDialog;
import com.somee.interestschat.models.Conversation;

import java.util.List;

/**
 * Created by Alexander on 30.10.2016.
 */
public class ConversationsAdapter extends BaseAdapter {
    private List<Conversation> items;
    private Activity context;
    private ConversationSelectionDialog dialog;

    public ConversationsAdapter(Activity context, ConversationSelectionDialog dialog, List<Conversation> items) {
        this.context = context;
        this.dialog = dialog;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final Conversation item = items.get(i);

        LayoutInflater layoutInflater = context.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.select_conversation_list_item, null);

        TextView login = (TextView) view.findViewById(R.id.txtConversationName);
        login.setText(item.getName());
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onListItemClick(item);
            }
        });

        ImageView avatarImage = (ImageView) view.findViewById(R.id.imgConversationLogo);
        avatarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onListItemClick(item);
            }
        });

        return view;
    }

    public void onListItemClick(Conversation conversation) {
        dialog.checkClick(conversation);
    }
}