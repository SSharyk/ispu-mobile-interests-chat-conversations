package com.somee.interestschat.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Alexander on 01.10.2016.
 */
public class User implements IFilterable {
    // region iId
    private int id;

    public int getId() {
        return this.id;
    }

    protected void setId(int value) {
        this.id = value;
    }
    // endregion

    // region Email
    private String email;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String value) {
        this.email = value;
    }
    // endregion

    // region Login
    private String login;

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String value) {
        this.login = value;
    }
    // endregion

    // region Password
    private String password;

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String value) {
        this.password = value;
    }
    // endregion

    // region Constructors
    public User(int id, String email, String login, String password) {
        setId(id);
        setEmail(email);
        setLogin(login);
        setPassword(password);
    }

    public User(JSONObject us) throws JSONException {
        setId(us.getInt("Id"));
        setEmail(us.getString("Email"));
        setLogin(us.getString("Login"));
    }

    @Override
    public boolean isSatisfy(CharSequence filterString) {
        return this.getLogin().toLowerCase().contains(filterString.toString().toLowerCase());
    }

    // endregion
}