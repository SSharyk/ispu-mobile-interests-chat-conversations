package com.somee.interestschat.dialogs;

import android.content.Context;
import android.widget.EditText;

import com.somee.interestschat.R;
import com.somee.interestschat.models.BoardItem;

/**
 * Created by Alexander on 14.11.2016.
 */
public class NoteEditingDialog extends ACustomDialog {
    private BoardItem item;

    public void setNote(BoardItem note) {
        this.item = note;
        setDialogContent();
    }

    public BoardItem getNote() {
        return this.item;
    }

    /**
     * General constructor
     * @param context Context of the dialog
     */
    public NoteEditingDialog(Context context) {
        super(context);
    }

    /**
     * Sets the main content of the dialog
     */
    @Override
    protected void setDialogContent() {
        this.setContentView(R.layout.board_item_edit_dialog);
        this.setTitle(R.string.board_item_edit_dialog_title);

        final EditText text = (EditText) this.findViewById(R.id.textfieldNoteTitle);
        if (item != null)
            text.setText(item.getTitle());
    }

    @Override
    protected void setDialogControlButtons() {
    }
}