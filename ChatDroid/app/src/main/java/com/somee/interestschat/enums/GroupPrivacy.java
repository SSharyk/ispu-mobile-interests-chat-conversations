package com.somee.interestschat.enums;

/**
 * Created by Alexander on 05.10.2016.
 */
public enum GroupPrivacy {
    Закрытая(2),
    Открытая(1);

    GroupPrivacy(int i){}

    public static String[] getEnumStringValues() {
        GroupPrivacy[] groupPrivacies = GroupPrivacy.values();
        String[] values = new String[groupPrivacies.length];
        for (int i = 0; i < groupPrivacies.length; i++) {
            values[i] = groupPrivacies[i].toString();
        }
        return values;
    }
}
