package com.somee.interestschat.models;

import com.somee.interestschat.enums.GroupPrivacy;
import com.somee.interestschat.fakes.GroupFakes;
import com.somee.interestschat.helpers.BasicDataService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alexander on 30.09.2016.
 * Describes each converstaion in database
 */
public class Conversation implements IFilterable {

    // region Id
    private int id;

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }
    //endregion

    // region Name
    private String name;

    public String getName() {
        return this.name;
    }

    private void setName(String value) {
        this.name = value;
    }
    // endregion

    // region GroupId
    private int groupId;

    public int getGroupId() {
        return this.groupId;
    }

    private void setGroupId(int groupId) {
        this.groupId = groupId;
    }
    // endregion

    // region Tags
    private String tags;

    public String getTags() {
        return tags;
    }

    public String[] getTagsAsList() {
        return tags.split(",");
    }

    private void setTags(String tags) {
        this.tags = tags;
    }
    // endregion

    // region PrivacyId
    private int privacyId;

    public int getPrivacyId() {
        return this.privacyId;
    }

    private void setPrivacyId(int privacyId) {
        this.privacyId = privacyId;
    }
    // endregion

    // region Privacy
    private GroupPrivacy privacy;

    public GroupPrivacy getPrivacy() {
        return privacy;
    }

    public void setPrivacy(GroupPrivacy privacy){
        this.privacy = privacy;
    }
    // endregion

    // region Votes
    private int votes;

    public int getVotes() {
        return votes;
    }

    private void setVotes(int votes) {
        this.votes = votes;
    }
    // endregion

    // region Messages
    private ArrayList<Message> messages;

    public ArrayList<Message> getMessages() {
        if (messages == null) {
            messages = new ArrayList<Message>();
            try {
                String path = "http://chatapi.somee.com/api/conversation/" + getId();
                String response = new BasicDataService().execute(path).get();
                JSONArray messages_json = new JSONArray(response);
                for(int i=0;i<messages_json.length();i++){
                    JSONObject mes_json = messages_json.getJSONObject(i);
                    Message message = new Message(mes_json);
                    this.messages.add(message);
                }
            } catch (Exception e) {
            }
        }
        return this.messages;
    }

    private void setMessages(ArrayList<Message> value) {
        this.messages = value;
    }
    // endregion

    // region LastMessageId
    private int lastMessageId;

    public int getLastMessageId() {
        return lastMessageId;
    }

    private void setLastMessageId(int lastMessageId) {
        this.lastMessageId = lastMessageId;
    }
    //endregion

    // region LastMessage
    public Message getLastMessage() {
        return messages.get(messages.size() - 1);
    }

    private String lastMessageText;

    public String getLastMessageText() {
        return lastMessageText;
    }

    protected void setLastMessage(String lastMessage) {
        this.lastMessageText = lastMessage;
    }
    // endregion

    // region Constructors
    public Conversation(int id, String name, GroupPrivacy privacy, ArrayList<Message> messages) {
        setId(id);
        setName(name);
        setPrivacy(privacy);
        setMessages(messages);
    }

    public Conversation(JSONObject modelJson) throws JSONException {
        try {
            setId(modelJson.getInt("Id"));
            setName(modelJson.getString("Name"));
            setVotes(modelJson.getInt("Votes"));
            setTags(modelJson.getString("Tags"));

            setPrivacyId(modelJson.getInt("PrivacyId"));
            setPrivacy(GroupPrivacy.values()[getPrivacyId()-1]);

            setGroupId(modelJson.getInt("GroupId"));

            setLastMessageId(modelJson.getInt("LastMessageId"));
            setLastMessage(modelJson.getString("LastMessage"));
        }
        catch (JSONException e){
            throw e;
        }
    }
    // endregion

    @Override
    public boolean isSatisfy(CharSequence filterString) {
        return this.getName().toLowerCase().contains(filterString.toString().toLowerCase());
    }

}