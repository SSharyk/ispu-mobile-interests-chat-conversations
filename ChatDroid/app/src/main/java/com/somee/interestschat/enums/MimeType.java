package com.somee.interestschat.enums;

/**
 * Created by Alexander on 05.10.2016.
 */
public enum MimeType {
    Note(0),

    Pdf(1),

    Jpg(2),

    Any(3);

    public static String[] getValuesAsStringArray() {
        MimeType[] items = MimeType.values();
        String[] vals = new String[items.length];
        for (int i = 0; i < items.length; i++) {
            vals[i] = items[i].toString();
        }
        return vals;
    }

    MimeType(int i) {
    }

    public static MimeType getById(int i) {
        return MimeType.values()[i];
    }
}