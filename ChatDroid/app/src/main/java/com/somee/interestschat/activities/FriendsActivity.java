package com.somee.interestschat.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.somee.interestschat.R;
import com.somee.interestschat.adapters.UsersAdapter;
import com.somee.interestschat.controllers.NavigationController;
import com.somee.interestschat.controllers.ToolbarController;
import com.somee.interestschat.dialogs.UserSelectionDialog;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;
import com.somee.interestschat.models.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FriendsActivity extends AppCompatActivity {

    private ToolbarController toolbarController;
    private ListView usersList;
    private Button cmdFindFriends;
    private NavigationController navigationController;

    /**
     * Calls on creation the activity
     * @param savedInstanceState Bundle object for restoring the state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends);

        this.toolbarController = new ToolbarController(this, "Друзья");
        navigationController = new NavigationController(this, this.getLocalClassName());

        this.usersList = (ListView) findViewById(R.id.lstUsers);
        this.cmdFindFriends = (Button) findViewById(R.id.cmdFindFriends);
        this.setListView();
    }

    /**
     * Retrieves friends from the server
     */
    private void setListView() {
        ArrayList<User> users = new ArrayList<>();
        try {
            String path = Global.ServerConnectionString + "/user/friends/"+Global.CurrentUser;
            String response = new BasicDataService().execute(path).get();
            JSONArray array = new JSONArray(response);
            for (int i=0;i<array.length();i++){
                JSONObject us_json = array.getJSONObject(i);
                User user = new User(us_json);
                users.add(user);
            }
        } catch (Exception e) {
        }
        usersList.setAdapter(new UsersAdapter(this, false, null, users));
    }

    /**
     * Calls on searching for new friends
     * @param view "Find more" button
     */
    public void onFindFriendsButtonClick(View view) {
        UserSelectionDialog dialog = new UserSelectionDialog(this);
        dialog.show();
    }
}