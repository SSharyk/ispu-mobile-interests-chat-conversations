package com.somee.interestschat.controllers;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.somee.interestschat.R;
import com.somee.interestschat.activities.InvitesActivity;
import com.somee.interestschat.activities.RegistrationActivity;
import com.somee.interestschat.dialogs.ConversationSelectionDialog;
import com.somee.interestschat.fakes.Global;

/**
 * Created by Alexander on 01.10.2016.
 */

/**
 * Represents toolbar for activities
 */
public class ToolbarController
        implements View.OnClickListener {
    private AppCompatActivity context;
    private ImageButton ToolbarAccount;
    private Toolbar MenuToolbar;
    private Button ToolbarBack;
    private LinearLayout ToolbarAccountLinearLayout;
    private TextView ToolbarNameView;
    private static TextView ToolbarInvitesCount;
    private String NameView;

    /**
     * Creates the controller
     * @param context Activity as a context
     * @param NameView View name for the representation
     */
    public ToolbarController(AppCompatActivity context, String NameView) {
        this.context = context;
        this.NameView = NameView;
        GetViewItems();
        SubscribeViewItems();
        //SetView();
    }

    /**
     * Gets all view items
     */
    private void GetViewItems() {
        MenuToolbar = (Toolbar) context.findViewById(R.id.Toolbar);
        ToolbarAccount = (ImageButton) context.findViewById(R.id.ToolbarAccountImageButton);
        ToolbarBack = (Button) context.findViewById(R.id.ToolbarBackButton);
        ToolbarAccountLinearLayout = (LinearLayout) context.findViewById(R.id.ToolbarAccountLinearLayout);
        ToolbarNameView = (TextView) context.findViewById(R.id.ToolbarNameViewText);
        ToolbarNameView.setText((context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                ? NameView : "");

        ToolbarInvitesCount = (TextView) context.findViewById(R.id.txtInvitesCount);
        int newInvites = InvitesActivity.getNewInvitesCount();
        if (newInvites > 0) {
            ToolbarInvitesCount.setVisibility(View.VISIBLE);
            ToolbarInvitesCount.setText(String.valueOf(newInvites));
        } else {
            ToolbarInvitesCount.setVisibility(View.GONE);
        }
    }

    /**
     * Sets listeners for view items
     */
    private void SubscribeViewItems() {
        this.ToolbarBack.setOnClickListener(this);
        this.ToolbarAccount.setOnClickListener(this);
    }

    /**
     * Implements clicking on account button
     * Shows pop-up menu
     * @param view "Account" Image button
     */
    private void OnToolbarAccountClick(View view) {
        PopupMenu popup = new PopupMenu(context, ToolbarAccount);
        popup.getMenuInflater()
                .inflate(R.menu.account_actions_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getTitle().toString()) {
                    case "Приглашения": {
                        Intent intent = new Intent(context, InvitesActivity.class);
                        context.startActivity(intent);
                    }
                    case "Поиск бесед": {
                        ConversationSelectionDialog dialog = new ConversationSelectionDialog(context);
                        dialog.show();
                        break;
                    }
                    case "Выход": {
                        context.finish();
                        Global.CurrentUser = -1;
                        Intent intent = new Intent(context, RegistrationActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                        break;
                    }
                }
                return true;
            }
        });

        popup.show();
    }

    /**
     * Implements back button logic
     * @param view "Back" button
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ToolbarBackButton:
                context.finish();
                break;
            case R.id.ToolbarAccountImageButton:
                this.OnToolbarAccountClick(view);
                break;
        }
    }

    /**
     * Sets the count of unread invites
     * @param newInvites count for representing
     */
    public static void refreshInvitesCount(int newInvites){
        if (newInvites > 0) {
            ToolbarInvitesCount.setVisibility(View.VISIBLE);
            ToolbarInvitesCount.setText(String.valueOf(newInvites));
        } else {
            ToolbarInvitesCount.setVisibility(View.GONE);
        }
    }
}