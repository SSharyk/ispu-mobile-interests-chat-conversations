package com.somee.interestschat.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.somee.interestschat.R;
import com.somee.interestschat.models.Invite;

import java.util.ArrayList;

/**
 * Created by Alexander on 26.10.2016.
 */
public class InvitesAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<Invite> invitesList;

    public InvitesAdapter(Activity context, ArrayList<Invite> invitesList) {
        this.context = context;
        this.invitesList = invitesList;
    }

    @Override
    public int getCount() {
        return invitesList.size();
    }

    @Override
    public Object getItem(int i) {
        return invitesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        Invite item = invitesList.get(i);

        LayoutInflater layoutInflater = context.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.invite_item, null);

        int entityId = item.getGoalConversationId();

        TextView user = (TextView) view.findViewById(R.id.textfieldInviteUserMessage);
        user.setText("Пользователь " + item.getFromUserName() + " приглашает Вас в " + ((entityId == -1) ? "друзья" : "беседу"));
        if (entityId != -1) {
            TextView conv = (TextView) view.findViewById(R.id.textfieldInviteConversationName);
            conv.setText("\"" + item.getGoalConversationName() + "\"");
        }
        return view;
    }
}
