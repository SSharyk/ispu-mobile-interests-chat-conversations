package com.somee.interestschat.controllers;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by Alexander on 08.10.2016.
 */

/**
 * Class that animates drawer layout
 */
public class MyActionBarDrawerToggle extends ActionBarDrawerToggle {
    private AppCompatActivity mHostActivity;
    private String viewName;
    private int mOpenedResource;
    private int mClosedResource;

    /**
     * Constructor for the class
     * @param host  Context activity
     * @param drawerLayout  Activity's DrawerLayout
     * @param openedResource    Source
     * @param closedResource    Target
     * @param viewName  View name for presentation
     */
    public MyActionBarDrawerToggle(AppCompatActivity host, DrawerLayout drawerLayout, int openedResource, int closedResource, String viewName)
    {
        super(host, drawerLayout, openedResource, closedResource);
        mHostActivity = host;
        mOpenedResource = openedResource;
        mClosedResource = closedResource;
        this.viewName = viewName;
        mHostActivity.getSupportActionBar().setTitle(viewName);
    }

    /**
     * Calls on opening the DrawerLayout
     * @param drawerView Context activity's DrawerLayout
     */
    @Override
    public void onDrawerOpened(View drawerView)
    {
        int drawerType = (int)drawerView.getTag();
        if (drawerType == 0)
        {
            super.onDrawerOpened(drawerView);
            mHostActivity.getSupportActionBar().setTitle(viewName);
        }
    }

    /**
     * Calls on hiding the DrawerLayout
     * @param drawerView Context activity's DrawerLayout
     */
    @Override
    public void onDrawerClosed(View drawerView)
    {
        int drawerType = (int)drawerView.getTag();

        if (drawerType == 0)
        {
            super.onDrawerClosed(drawerView);
            mHostActivity.getSupportActionBar().setTitle(mClosedResource);
        }
    }

    /**
     * Calls on sliding the DrawerLayout
     * @param drawerView Context activity's DrawerLayout
     */
    @Override
    public void onDrawerSlide(View drawerView, float slideOffset)
    {
        int drawerType = (int)drawerView.getTag();
        if (drawerType == 0)
        {
            super.onDrawerSlide(drawerView, slideOffset);
        }
    }
}
