package com.somee.interestschat.helpers;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.somee.interestschat.R;


/**
 * Created by Alexander on 17.11.2016.
 */

/**
 * Class that implements convenient notifications builder
 */
public class NotificationsHelper {
    public static void sendNotification(int notificationId, Context context, Class<? extends Activity> parentActivity, String message){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle("New messages")
                        .setContentText(message);
        Intent resultIntent = new Intent(context, parentActivity);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);  // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(parentActivity);        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notificationId, mBuilder.build());
    }
}
