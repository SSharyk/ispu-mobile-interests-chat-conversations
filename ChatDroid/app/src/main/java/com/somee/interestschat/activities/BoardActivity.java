package com.somee.interestschat.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.somee.interestschat.R;
import com.somee.interestschat.adapters.BoardItemsAdapter;
import com.somee.interestschat.controllers.NavigationController;
import com.somee.interestschat.controllers.ToolbarController;
import com.somee.interestschat.dialogs.NoteEditingDialog;
import com.somee.interestschat.enums.MimeType;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;
import com.somee.interestschat.models.BoardItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Activity represents notes on the conversation's board
 */
public class BoardActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private ListView boardItemsList;
    private Spinner mimeTypeSpinner;
    private EditText searchField;
    private NavigationController navigationController;
    private ToolbarController toolbarController;
    private HashMap<MimeType, ArrayList<BoardItem>> boardItems;
    private ArrayList<BoardItem> allItems;
    private final Activity thisContext = this;
    private MimeType selectedMime = MimeType.Any;
    private CharSequence filterString = "";
    private BoardItemsAdapter adapter;
    private NoteEditingDialog dialog;
    private BoardItem selectedBoardItem;

    private AlertDialog.Builder dialogBuilder;
    private static final String acceptButton = "Удалить";
    private static final String cancelButton = "Оставить на доске";
    private static final String backButton = "Назад";

    /**
     * Calls on creation the activity
     * @param savedInstanceState Bundle object for restoring the state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board);

        this.boardItemsList = (ListView) findViewById(R.id.lstBoardItems);
        setListView();

        navigationController = new NavigationController(this, this.getLocalClassName());
        this.toolbarController = new ToolbarController(this, "Доска беседы");

        this.mimeTypeSpinner = (Spinner) this.findViewById(R.id.comboMimeTypes);
        setMimeTypeSpinner();

        this.searchField = (EditText) findViewById(R.id.txtBoardItemSearch);
        setFilterListener(searchField);

        this.boardItemsList.setOnItemClickListener(this);
        this.boardItemsList.setOnItemLongClickListener(this);
    }

    /**
     * Retrieves board items from the server
     */
    private void setListView() {
        ArrayList<BoardItem> boardItems = new ArrayList<>();
        try {
            String path = Global.ServerConnectionString + "/board/in/" + Global.CurrentConversation.getId();
            String response = new BasicDataService().execute(path).get();
            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject bi_json = array.getJSONObject(i);
                BoardItem boardItem = new BoardItem(bi_json);
                boardItems.add(boardItem);
            }
        } catch (Exception e) {
        }

        this.allItems = boardItems;
        this.boardItems = convertListToLookup(boardItems);
        this.adapter = new BoardItemsAdapter(this, this.boardItems);
        boardItemsList.setAdapter(adapter);
    }

    /**
     * Converts list to the lookup
     * @param list Input data
     * @return Equivalent lookup
     */
    private HashMap<MimeType, ArrayList<BoardItem>> convertListToLookup(ArrayList<BoardItem> list) {
        HashMap<MimeType, ArrayList<BoardItem>> dictionary = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            BoardItem item = list.get(i);
            MimeType mime = item.getMimeType();
            if (!dictionary.containsKey(mime)) {
                dictionary.put(mime, new ArrayList<BoardItem>());
            }
            dictionary.get(mime).add(item);
        }
        return dictionary;
    }

    /**
     * Retreives MIME types references from the server
     */
    private void setMimeTypeSpinner() {
        ArrayAdapter<CharSequence> mimeTypeAdapter = new ArrayAdapter<CharSequence>(this,
                android.R.layout.simple_spinner_item,
                MimeType.getValuesAsStringArray());
        mimeTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mimeTypeSpinner.setAdapter(mimeTypeAdapter);
        mimeTypeSpinner.setSelection(mimeTypeAdapter.getCount() - 1);

        mimeTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedMime = MimeType.getById(i);
                applyFilter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /**
     * Listens for changing the filter edit text
     * @param searchField Sender
     */
    private void setFilterListener(EditText searchField) {
        if (searchField != null) {
            searchField.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    filterString = editable;
                    applyFilter();
                }
            });
        }
    }

    /**
     * Changes the viewing list in according the filter
     */
    private void applyFilter() {
        HashMap<MimeType, ArrayList<BoardItem>> dictionary = new HashMap<>();
        for (int i = 0; i < allItems.size(); i++) {
            BoardItem item = allItems.get(i);
            MimeType mime = item.getMimeType();
            if ((selectedMime == MimeType.Any || mime == selectedMime) && item.isSatisfy(filterString)) {
                if (!dictionary.containsKey(mime)) {
                    dictionary.put(mime, new ArrayList<BoardItem>());
                }
                dictionary.get(mime).add(item);
            }
        }

        this.adapter = new BoardItemsAdapter(thisContext, dictionary);
        boardItemsList.setAdapter(adapter);
    }

    /**
     * Overrides standart listener for item click event
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Object selectedItem = adapter.getItem(i);
        if (adapter.GetItemViewType(i) != 0)
            this.selectedBoardItem = (BoardItem) selectedItem;

        this.dialog = new NoteEditingDialog(this);
        dialog.setNote(selectedBoardItem);
        dialog.show();
    }

    /**
     * Calls on confirmation of "Create/Edit note" dialog
     * Saves note in the server and refreshes list
     * @param view OK button
     */
    public void onOkNoteEditingDialog(View view) {
        String text = ((EditText) dialog.findViewById(R.id.textfieldNoteTitle)).getText().toString();
        if (text.length() == 0) {
            Toast.makeText(getApplicationContext(), "Заметка не должна быть пустой", Toast.LENGTH_LONG).show();
            return;
        }

        text = text.replace(' ', '_');
        this.dialog.cancel();
        try {
            String path = Global.ServerConnectionString + "/board/" +
                    ((dialog.getNote().getId() != -1)
                            ? "updatenote/" + dialog.getNote().getId() + "/" + text
                            : "createnote/" + Global.CurrentConversation.getId() + "/" + text + "/" + Global.CurrentUser);
            new BasicDataService().execute(path).get();
            this.setListView();
        } catch (Exception e) {
        }
    }

    /**
     * Just closes the dialog
     * @param view "Cancel" button
     */
    public void onCancelNoteEditingDialog(View view) {
        this.dialog.cancel();
    }

    /**
     * Shows "Create/Edit note" dialog
     * @param view "New note"button
     */
    public void onCreateNote(View view) {
        this.dialog = new NoteEditingDialog(thisContext);
        dialog.setNote(new BoardItem());
        dialog.show();
    }

    /**
     * Overrides standart listener for item long click event
     * Shows confirmation dialog for deleting note
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        final BoardItem selectedNote = (BoardItem) this.adapter.getItem(i);
        dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Удаление");
        dialogBuilder.setCancelable(true);

        String dialogMessage = "Вы действительно хотите удалить заметку \r\n";
        dialogMessage += selectedNote.getTitle().substring(0, Math.min(40, selectedNote.getTitle().length()));
        dialogMessage += "...";
        dialogBuilder.setMessage(dialogMessage);

        dialogBuilder.setPositiveButton(acceptButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                try {
                    String path = Global.ServerConnectionString + "/board/deletenote/" + selectedNote.getId();
                    new BasicDataService().execute(path).get();
                    setListView();
                } catch (Exception e) {
                    Log.e("mConn", e.getMessage());
                }
            }
        });
        dialogBuilder.setNeutralButton(backButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        dialogBuilder.show();

        return true;
    }
}