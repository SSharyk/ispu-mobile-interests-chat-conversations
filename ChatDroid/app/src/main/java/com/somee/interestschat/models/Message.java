package com.somee.interestschat.models;

import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Alexander on 30.09.2016.
 * Describes each conversation message in database
 */
public class Message {
    // region Id
    private int id;

    public int getId() {
        return this.id;
    }

    private void setId(int value) {
        this.id = value;
    }
    // endregion

    // region Text
    private String messageText;

    public String getMessageText() {
        return this.messageText;
    }

    private void setMessageText(String value) {
        this.messageText = value;
    }
    // endregion

    // region User
    private int userId;

    public int getUserId() {
        return userId;
    }

    /// TODO: change accessibility level (now it is used for fakes)
    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        try {
            String path = "http://chatapi.somee.com/api/user/" + getUserId();
            String response = new BasicDataService().execute(path).get();
            JSONObject user_json = new JSONObject(response);
            return new User(user_json);
        } catch (Exception e) {
        }
        return null;
    }
    // endregion

    // region ConversationId
    private int conversationId;

    public int getConversationId() {
        return conversationId;
    }

    private void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }
    // endregion

    // region Date
    private Date postDateTime;

    public Date getPostDateTime() {
        return postDateTime;
    }

    private void setPostDateTime(Date postDateTime) {
        this.postDateTime = postDateTime;
    }
    // endregion

    // region Constructors
    public Message(int id, String message, int userId, int conversationId) {
        setId(id);
        setMessageText(message);
        setPostDateTime(new Date());
        setUserId(userId);
        setConversationId(conversationId);
    }

    public Message(int id){
        this(id, "Simple fake message # " + id, Global.CurrentUser + 1, 1);
    }

    public Message(JSONObject mes) throws JSONException, ParseException {
        setId(mes.getInt("Id"));
        setMessageText(mes.getString("Text"));
        setConversationId(mes.getInt("ConversationId"));
        setUserId(mes.getInt("UserId"));
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        setPostDateTime(format.parse(mes.getString("PostDateTime")));
    }
    // endregion
}
