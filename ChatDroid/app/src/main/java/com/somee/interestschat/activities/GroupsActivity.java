package com.somee.interestschat.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.somee.interestschat.R;
import com.somee.interestschat.adapters.GroupsAdapter;
import com.somee.interestschat.controllers.NavigationController;
import com.somee.interestschat.controllers.ToolbarController;
import com.somee.interestschat.dialogs.ACustomSelectionDialog;
import com.somee.interestschat.dialogs.ConversationCreatingDialog;
import com.somee.interestschat.dialogs.ConversationSelectionDialog;
import com.somee.interestschat.fakes.GroupFakes;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;
import com.somee.interestschat.models.Conversation;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class GroupsActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener {

    private GroupsAdapter adapter;
    private ListView groupsList;
    private ToolbarController toolbarController;
    private ConversationCreatingDialog dialog;
    private NavigationController navigationController;

    /**
     * Calls on creation the activity
     * @param savedInstanceState Bundle object for restoring the state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.groups);

        navigationController = new NavigationController(this, this.getLocalClassName());
        this.toolbarController = new ToolbarController(this, "Беседы");
        this.groupsList = (ListView) findViewById(R.id.lstGroups);
        this.setListView();
        this.groupsList.setOnItemClickListener(this);
    }

    /**
     * Retrieves conversations from the server
     */
    private void setListView(){
        HashMap<String, ArrayList<Conversation>> groups = new HashMap<>();
        try {
            String path = Global.ServerConnectionString + "/conversation/byGroups/" + Global.CurrentUser;
            String response = (new BasicDataService()).execute(path).get();
            JSONObject map = new JSONObject(response);
            for(int nameId = 0; nameId<map.names().length(); nameId++) {
                JSONArray name = map.getJSONArray(map.names().getString(nameId));
                int groupId = map.names().getInt(nameId);
                ArrayList<Conversation> list=new ArrayList<>();
                for(int modelId=0;modelId<name.length();modelId++) {
                    JSONObject conv = name.getJSONObject(modelId);
                    Conversation conversation = new Conversation(conv);
                    list.add(conversation);
                }
                groups.put(GroupFakes.getCategories()[groupId-1], list);
            }
        }
        catch (Exception e){
        }

        this.adapter = new GroupsAdapter(this, groups);
        this.groupsList.setAdapter(adapter);
    }

    /**
     * Shows dialog for creating
     * @param view "Create new" button
     */
    public void onCreateConverstaion(View view) {
        this.dialog = new ConversationCreatingDialog(this);
        dialog.show();
    }

    /**
     * Shows dialog for searching existing conversations
     * @param view "Find more" button
     */
    public void onFindMoreConversations(View view) {
        ACustomSelectionDialog<Conversation> dialog = new ConversationSelectionDialog(this);
        dialog.show();
    }

    /**
     * Saves new conversation on the server
     * @param view "Create" button
     */
    public void onOkConversationCreationDialog(View view) {
        int privacyId = ((Spinner) dialog.findViewById(R.id.comboPrivacy)).getSelectedItemPosition() + 1;
        int categoryId = ((Spinner) dialog.findViewById(R.id.comboCategory)).getSelectedItemPosition() + 1;
        String name = ((TextView) dialog.findViewById(R.id.txtGroupName)).getText().toString();
        if (name.length() == 0) {
            Toast.makeText(getApplicationContext(), "Название беседы не должно быть пустым", Toast.LENGTH_LONG).show();
            return;
        }
        name = name.replace(' ', '_');
        this.dialog.cancel();
        try {
            String path = Global.ServerConnectionString + "/conversation/create/" +
                    name + "/" + privacyId + "/" + categoryId + "/" + Global.CurrentUser;
            new BasicDataService().execute(path).get();
            this.setListView();
        } catch (Exception e) {
        }
    }

    /**
     * Just closes creation dialog
     * @param view "Cancel" button
     */
    public void onCancelConversationCreationDialog(View view) {
        this.dialog.cancel();
    }

    /**
     * Overrides standart listener for item click event
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Object selectedItem = adapter.getItem(i);
        Conversation selectedConversation = null;
        if (adapter.GetItemViewType(i) != 0)
            selectedConversation = (Conversation) selectedItem;
        Global.CurrentConversation = selectedConversation;

        Intent intent = new Intent(getApplicationContext(), ConversationActivity.class);
        startActivity(intent);
    }

    public void updateList(){
        this.setListView();
    }
}
