package com.somee.interestschat.helpers;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Alexander on 17.10.2016.
 */

/**
 * Class that implements logic for the server interaction
 */
public class BasicDataService extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... urls) {
        StringBuilder buf = new StringBuilder();

        try {
            URL url = new URL(urls[0]);

            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET"); // установка метода получения данных -GET
            c.setReadTimeout(10000); // установка таймаута перед выполнением - 10 000 миллисекунд
            c.connect(); // подключаемся к ресурсу

            BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));

            String line = null;
            while ((line = reader.readLine()) != null) {
                buf.append(line + "\n");
            }
            return buf.toString();
        } catch (Exception e) {
            return buf.toString();
        }
    }
}