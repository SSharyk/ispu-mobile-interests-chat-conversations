package com.somee.interestschat.fakes;

import com.somee.interestschat.helpers.BasicDataService;

import org.json.JSONArray;

/**
 * Created by Alexander on 03.10.2016.
 */
public class GroupFakes {
    private static String[] _categories;

    public static String[] getCategories(){
        if (_categories==null || (_categories.length==1 && _categories[0]=="Unknown"))
            getCategoriesFromServer();
        return _categories;
    }

    private static void getCategoriesFromServer(){
        try {
            String path = "http://chatapi.somee.com/api/conversation/categories";
            String response = (new BasicDataService()).execute(path).get();
            JSONArray categories = new JSONArray(response);
            _categories = new String[categories.length()];
            for(int i=0;i<categories.length();i++){
                _categories[i]=categories.getString(i);
            }
        }
        catch (Exception e){
            _categories = new String[1];
            _categories[0] = "Unknown";
        }
    }
}
