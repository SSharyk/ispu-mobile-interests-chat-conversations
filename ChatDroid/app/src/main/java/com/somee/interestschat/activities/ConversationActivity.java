package com.somee.interestschat.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.somee.interestschat.R;
import com.somee.interestschat.adapters.MessagesAdapter;
import com.somee.interestschat.controllers.NavigationController;
import com.somee.interestschat.controllers.ToolbarController;
import com.somee.interestschat.dialogs.UserSelectionDialog;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;
import com.somee.interestschat.helpers.NotificationsHelper;
import com.somee.interestschat.models.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class ConversationActivity extends AppCompatActivity {

    private ListView messagesList;
    private ToolbarController toolbarController;
    private NavigationController navigationController;
    private ImageButton cmdActionMenu;
    private ArrayList<Message> messages;
    private final Activity thisContext = this;
    private static final int REFRESHING_INTERVAL = 15;
    private static final int NOTIFICATION_ID = (new Random()).nextInt();
    private int unreadMessages;

    /**
     * Calls on creation the activity
     * @param savedInstanceState Bundle object for restoring the state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conversation);

        this.toolbarController = new ToolbarController(this, "Сообщения");
        navigationController = new NavigationController(this, this.getLocalClassName());

        this.cmdActionMenu = (ImageButton) findViewById(R.id.cmdConversationActionMenu);
        this.messagesList = (ListView) findViewById(R.id.lstMessages);
        setListView();
        unreadMessages = 0;

        /// Implementation the separate thread for scanning new messages in the conversation
        /// and refreshing the list if necessary
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int position = messagesList.getFirstVisiblePosition();
                ArrayList<Message> newMessages = getMessagesList();
                int old = messagesList.getCount();
                if (newMessages.size() > old) {
                    messages = new ArrayList<>(newMessages);

                    final MessagesAdapter adapter = new MessagesAdapter(thisContext, newMessages);
                    messagesList.setAdapter(adapter);
                    messagesList.setSelection(position);

                    unreadMessages = newMessages.size() - old;
                    for (int i = old; i < newMessages.size(); i++)
                        if (newMessages.get(i).getUserId() == Global.CurrentUser)
                            unreadMessages--;
                    if (unreadMessages > 0)
                        NotificationsHelper.sendNotification(NOTIFICATION_ID, thisContext, ConversationActivity.class,
                                "You have received " + unreadMessages + " new messages in " + Global.CurrentConversation.getName());
                }
                handler.postDelayed(this, REFRESHING_INTERVAL * 1000);
            }
        }, REFRESHING_INTERVAL * 1000);
    }

    /**
     * Sets the list of entities
     */
    private void setListView() {
        messages = getMessagesList();

        final MessagesAdapter adapter = new MessagesAdapter(this, messages);
        this.messagesList.setAdapter(adapter);
        messagesList.setSelection(messages.size() - 1);
    }

    /**
     * Retrieves messages from the server
     */
    private ArrayList<Message> getMessagesList() {
        ArrayList<Message> messagesL = new ArrayList<>();
        try {
            String path = Global.ServerConnectionString + "/message/in/" + Global.CurrentConversation.getId();
            String response = new BasicDataService().execute(path).get();
            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject message_json = array.getJSONObject(i);
                Message message = new Message(message_json);
                messagesL.add(message);
                if (message.getUserId() == Global.CurrentUser)
                    unreadMessages++;
            }
        } catch (Exception e) {
        }
        return messagesL;
    }

    /**
     * Calls on creating new message
     * @param view Send button
     */
    public void onSendMessage(View view) {
        String text = ((EditText) findViewById(R.id.txtNewMessage)).getText().toString();
        try {
            String path = Global.ServerConnectionString + "/message/create/" +
                    Global.CurrentConversation.getId() + "/" + Global.CurrentUser + "/" + text.replace(' ', '_');
            new BasicDataService().execute(path).get();
            this.setListView();
            ((EditText) findViewById(R.id.txtNewMessage)).setText("");
        } catch (Exception e) {
        }
    }

    /**
     * Shows oprions for the conversation
     * @param view
     */
    public void onShowPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(ConversationActivity.this, cmdActionMenu);
        popup.getMenuInflater()
                .inflate(R.menu.conversation_actions_menu, popup.getMenu());

        final Activity ctx = this;
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getTitle().toString()) {
                    case "Пригласить друзей":
                        UserSelectionDialog dialog = new UserSelectionDialog(ctx);
                        dialog.show();
                        break;
                    case "К доске беседы":
                        Intent intent = new Intent(getApplicationContext(), BoardActivity.class);
                        startActivity(intent);
                        break;
                    case "Покинуть беседу":
                        try {
                            String path = Global.ServerConnectionString +
                                    "/conversation/leave/" + Global.CurrentUser + "/" + Global.CurrentConversation.getId();
                            String response = new BasicDataService().execute(path).get();
                            if (response.toUpperCase().contains("TRUE"))
                                ctx.finish();
                        } catch (Exception e) {
                        }
                        break;
                    case "Удалить беседу":
                        try {
                            String path = Global.ServerConnectionString +
                                    "/conversation/delete/" + Global.CurrentUser + "/" + Global.CurrentConversation.getId();
                            String response = new BasicDataService().execute(path).get();
                            if (response.toUpperCase().contains("TRUE"))
                                ctx.finish();
                        } catch (Exception e) {
                        }
                        break;
                }
                return true;
            }
        });

        popup.show();
    }
}