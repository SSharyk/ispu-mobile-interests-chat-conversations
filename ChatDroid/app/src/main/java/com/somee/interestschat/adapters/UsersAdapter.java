package com.somee.interestschat.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.somee.interestschat.R;
import com.somee.interestschat.dialogs.UserSelectionDialog;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;
import com.somee.interestschat.models.User;

import java.util.List;

/**
 * Created by Alexander on 01.10.2016.
 */
public class UsersAdapter extends BaseAdapter {
    private List<User> items;
    private boolean isSelectUsers;
    private UserSelectionDialog dialog;
    private Activity context;

    private CheckBox isSelected;

    public UsersAdapter(Activity context, boolean isSelectUsers, UserSelectionDialog dialog, List<User> items) {
        this.context = context;
        this.isSelectUsers = isSelectUsers;
        this.dialog = dialog;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        final User item = items.get(i);
        if (!isSelectUsers) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            View view = layoutInflater.inflate(R.layout.friend_item, null);

            TextView login = (TextView) view.findViewById(R.id.txtUserLogin);
            login.setText(item.getLogin());
            TextView commonCount = (TextView) view.findViewById(R.id.txtCommonConversationsCount);

            try {
                String commons = Global.ServerConnectionString + "/user/common" +
                        "/" + Global.CurrentUser + "/" + item.getId();
                String resp = (new BasicDataService()).execute(commons).get();
                commonCount.setText(resp);
            } catch (Exception e) {
                commonCount.setText("??");
            }
            return view;
        } else {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            View view = layoutInflater.inflate(R.layout.select_user_list_item, null);

            isSelected = (CheckBox) view.findViewById(R.id.checkSelectUser);

            TextView login = (TextView) view.findViewById(R.id.txtUserLogin);
            login.setText(item.getLogin());
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isSelected.toggle();
                    onListItemClick(item);
                }
            });

            ImageView avatarImage = (ImageView) view.findViewById(R.id.imgUserLogo);
            avatarImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isSelected.toggle();
                    onListItemClick(item);
                }
            });

            LinearLayout layout = (LinearLayout) view.findViewById(R.id.layoutSpecificUser);
            return view;
        }
    }

    public void onListItemClick(User user){
        dialog.checkClick(user);
    }
}