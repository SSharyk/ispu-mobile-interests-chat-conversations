package com.somee.interestschat.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.somee.interestschat.R;
import com.somee.interestschat.adapters.InvitesAdapter;
import com.somee.interestschat.controllers.NavigationController;
import com.somee.interestschat.controllers.ToolbarController;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;
import com.somee.interestschat.models.Invite;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class InvitesActivity extends AppCompatActivity
implements AdapterView.OnItemClickListener {

    private ToolbarController toolbarController;
    private NavigationController navigationController;
    private InvitesAdapter adapter;
    private ListView invitesList;
    private static int current;

    private AlertDialog.Builder dialogBuilder;
    private Activity context;
    private static final String dialogTitle = "Принять приглашение?";
    private static String dialogMessage = "Приглашение от пользователя ";
    private static final String acceptButton = "Принять";
    private static final String cancelButton = "Отклонить";
    private static final String backButton = "Назад";

    /**
     * Calls on creation the activity
     * @param savedInstanceState Bundle object for restoring the state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invites);

        this.toolbarController = new ToolbarController(this, "Приглашения");
        navigationController = new NavigationController(this, this.getLocalClassName());

        this.invitesList = (ListView) findViewById(R.id.lstInvites);
        this.setListView();
        this.invitesList.setOnItemClickListener(this);

        this.context = InvitesActivity.this;
    }

    /**
     * Retrieves invites from the server
     */
    private void setListView() {
        ArrayList<Invite> invites = new ArrayList<>();
        JSONArray array = new JSONArray();
        try {
            String path = Global.ServerConnectionString + "/invite/unread/" + Global.CurrentUser;
            String response = new BasicDataService().execute(path).get();
            array = new JSONArray(response);
        } catch (Exception e) {
        }

        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject invite_json = array.getJSONObject(i);
                Invite invite = new Invite(invite_json);
                invites.add(invite);
            } catch (Exception e) {
            }
        }
        this.adapter = new InvitesAdapter(this, invites);
        this.invitesList.setAdapter(adapter);
    }

    /**
     * Overrides standart listener for item click event
     * Shows confirmation dialog
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Invite selectedInvite = (Invite) adapter.getItem(i);
        InvitesActivity.current = selectedInvite.getId();

        dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(dialogTitle);
        dialogBuilder.setCancelable(true);

        dialogMessage = "Приглашение от пользователя ";
        dialogMessage += selectedInvite.getFromUserName();
        dialogMessage += " в " + ((selectedInvite.getGoalConversationId() == -1) ? "друзья" : ("беседу " + selectedInvite.getGoalConversationName()));
        dialogBuilder.setMessage(dialogMessage);

        dialogBuilder.setPositiveButton(acceptButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                sendInviteResponse("accept");
                toolbarController.refreshInvitesCount(invitesList.getCount());
            }
        });
        dialogBuilder.setNeutralButton(backButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        dialogBuilder.setNegativeButton(cancelButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                sendInviteResponse("cancel");
                toolbarController.refreshInvitesCount(invitesList.getCount());
            }
        });

        dialogBuilder.show();
    }

    /**
     * Sends response to the server
     * @param actionInvitePath URI
     */
    private void sendInviteResponse(String actionInvitePath) {
        try {
            String path = Global.ServerConnectionString + "/invite/" + actionInvitePath + "/" + current;
            new BasicDataService().execute(path).get();
        } catch (Exception e) {
        }
        setListView();
    }

    /**
     * Checks for unread invites
     * @return Count of unread invites
     */
    public static int getNewInvitesCount() {
        try {
            String path = Global.ServerConnectionString + "/invite/unread/" + Global.CurrentUser;
            String response = new BasicDataService().execute(path).get();
            JSONArray array = new JSONArray(response);
            return array.length();
        } catch (Exception e) {
        }
        return 0;
    }
}