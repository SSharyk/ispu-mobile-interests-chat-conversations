package com.somee.interestschat.adapters;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.somee.interestschat.R;
import com.somee.interestschat.enums.MimeType;
import com.somee.interestschat.models.BoardItem;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Alexander on 05.10.2016.
 */
public class BoardItemsAdapter extends BaseAdapter {
    private HashMap<MimeType, ArrayList<BoardItem>> boardItemsList;
    private Activity context;

    public BoardItemsAdapter(Activity context, HashMap<MimeType, ArrayList<BoardItem>> boardItemsList) {
        this.context = context;
        this.boardItemsList = boardItemsList;
    }

    @Override
    public int getCount() {
        int count = 0;
        for (ArrayList<BoardItem> group : boardItemsList.values())
            count += group.size() + 1;
        return count;
    }

    @Override
    public Object getItem(int i) {
        for (ArrayList<BoardItem> group : boardItemsList.values()) {
            int size = group.size() + 1;
            if (i == 0)
                return null;
            if (i < size)
                return group.get(i - 1);
            i -= size;
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = GetItemViewType(position);
        View view = convertView;
        if (type == 0) {
            view = context.getLayoutInflater().inflate(R.layout.board_section_header, null);
            setSectionHeader(view, position);
        } else {
            view = context.getLayoutInflater().inflate(R.layout.board_item, null);
            setBoardItem(view, position);
        }
        return view;
    }

    public int GetItemViewType(int position) {
        for (ArrayList<BoardItem> group : boardItemsList.values()) {
            int size = group.size() + 1;
            if (position == 0)
                return 0;
            if (position < size)
                return 1;
            position -= size;
        }
        throw new NullPointerException();
    }

    protected MimeType GetGroupName(int position) {
        for (MimeType key : boardItemsList.keySet()) {
            ArrayList<BoardItem> group = boardItemsList.get(key);
            int size = group.size() + 1;
            if (position < size)
                return key;
            position -= size;
        }
        throw new NullPointerException();
    }

    protected void setSectionHeader(View view, int position) {
        TextView boardSectionHeaderText = (TextView) view.findViewById(R.id.BoardSectionHeaderName);
        boardSectionHeaderText.setText(GetGroupName(position).toString().toUpperCase());
    }

    protected void setBoardItem(View view, int position) {
        BoardItem item = (BoardItem) this.getItem(position);

        ImageView image = (ImageView) view.findViewById(R.id.BoardItemImage);
        int srcId;
        switch (item.getMimeType()) {
            case Jpg:
                srcId = R.drawable.icon_jpg;
                break;
            case Note:
                srcId = R.drawable.icon_note;
                break;
            case Pdf:
                srcId = R.drawable.icon_pdf;
                break;
            default:
                throw new Resources.NotFoundException("icon_" + item.getMimeType().toString().toLowerCase());
        }
        image.setImageResource(srcId);

        TextView title = (TextView) view.findViewById(R.id.BoardItemName);
        title.setText(item.getTitle());
    }
}