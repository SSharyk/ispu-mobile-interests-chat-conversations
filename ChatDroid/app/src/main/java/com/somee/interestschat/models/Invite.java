package com.somee.interestschat.models;

import com.somee.interestschat.enums.InviteStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Alexander on 05.10.2016.
 */
public class Invite {
    // region Id
    private int id;

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }
    //endregion

    // region FromUserId
    private int fromUserId;

    public int getFromUserId() {
        return fromUserId;
    }

    private void setFromUserId(int fromUserId) {
        this.fromUserId = fromUserId;
    }
    // endregion

    // region FromUserName
    private String fromUserName;

    public String getFromUserName() {
        return fromUserName;
    }

    private void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }
    // endregion

    // region ToUserId
    private int toUserId;

    public int getToUserId() {
        return toUserId;
    }

    private void setToUserId(int toUserId) {
        this.toUserId = toUserId;
    }
    // endregion

    // region Conversation
    private int goalConversationId;

    public int getGoalConversationId() {
        return goalConversationId;
    }

    private void setGoalConversationId(int goalConversationId) {
        this.goalConversationId = goalConversationId;
    }
    // endregion

    // region Conversation
    private String goalConversationName;

    public String getGoalConversationName() {
        return goalConversationName;
    }

    private void setGoalConversationName(String goalConversationName) {
        this.goalConversationName = goalConversationName;
    }
    // endregion

    // region Status
    private InviteStatus status;

    public InviteStatus getStatus() {
        return status;
    }

    private void setStatus(InviteStatus status) {
        this.status = status;
    }
    // endregion

    // region Date
    private Date sendDate;

    public Date getSendDate() {
        return sendDate;
    }

    private void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }
    // endregion

    // region Constructors
    public Invite(int id, int from, int to, int conversation, InviteStatus status, Date sendDate){
        setId(id);
        setFromUserId(from);
        setToUserId(to);
        setGoalConversationId(conversation);
        setStatus(status);
        setSendDate(sendDate);
    }

    public Invite(JSONObject inv) throws JSONException, ParseException {
        setId(inv.getInt("Id"));
        setFromUserId(inv.getInt("FromUserId"));
        setFromUserName(inv.getString("FromUserName"));
        setToUserId(inv.getInt("ToUserId"));
        setGoalConversationId(inv.getInt("EntityId"));
        setGoalConversationName(inv.getString("EntityName"));
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        setSendDate(format.parse(inv.getString("SendDate")));
    }
    // endregion
}
