package com.somee.interestschat.dialogs;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.somee.interestschat.R;
import com.somee.interestschat.models.IFilterable;

import java.util.ArrayList;

/**
 * Created by Alexander on 30.10.2016.
 */

/**
 * Dialog for selecting entities
 * @param <T> Class that implements IFilterable for searching
 */
public abstract class ACustomSelectionDialog<T extends IFilterable> extends ACustomDialog {
    protected int pageNumber;
    private static final int PAGE_LENGTH = 10;
    protected int TotalSelected;

    protected Button okButton;
    protected Button cancelButton;

    protected ArrayList<T> allEntities;
    protected ArrayList<T> entitiesList;
    protected ListView entitiesListView;
    protected ArrayList<T> selectedEntities;

    /**
     * Getter for selected entities list
     * @return Selected entities list
     */
    public ArrayList<T> getSelectedEntities() {
        if (this.selectedEntities == null)
            selectedEntities = new ArrayList<>();
        return this.selectedEntities;
    }

    /**
     * General constructor for the all custom dialogs
     * @param context Context of the dialog
     */
    public ACustomSelectionDialog(Context context) {
        super(context);
    }

    /**
     * Sets the list of entities as a content
     * @param list All required entities
     */
    protected abstract void setListView(ArrayList<T> list);

    /**
     * Sets view items on the pagination bar
     */
    protected void setPaginationButtons() {
        final Button cmdFirst = (Button) this.findViewById(R.id.cmdToTheFirst);
        cmdFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageNumber = 0;
                setListView(null);
            }
        });

        final Button cmdPrev = (Button) this.findViewById(R.id.cmdToPrevious);
        cmdPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pageNumber > 0)
                    pageNumber--;
                setListView(null);
            }
        });

        final Button cmdNext = (Button) this.findViewById(R.id.cmdToNext);
        cmdNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pageNumber < getTotalPagesCount())
                    pageNumber++;
                setListView(null);
            }
        });

        final Button cmdLast = (Button) this.findViewById(R.id.cmdToTheLast);
        cmdLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageNumber = getTotalPagesCount();
                setListView(null);
            }
        });

        final EditText searchField = (EditText) this.findViewById(R.id.txtSearchField);
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence searchLine, int i, int i1, int i2) {
                ArrayList<T> list = new ArrayList<T>();
                for (T item : allEntities) {
                    if (item.isSatisfy(searchLine)) {
                        list.add(item);
                    }
                }

                setListView(list);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        final EditText editPageField = (EditText) this.findViewById(R.id.txtPageNumber);
        editPageField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String txt = editable.toString();
                int page = (txt.length() == 0) ? pageNumber : Integer.parseInt(txt) - 1;
                if (page == pageNumber)
                    return;
                if (page > 0 && page < getTotalPagesCount()) {
                    pageNumber = page;
                    setListView(null);
                } else {
                    Toast.makeText(context, "Page is invalid", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Calculates total count of pages
     * @return Total count of pages
     */
    protected int getTotalPagesCount() {
        int count = entitiesList.size() / PAGE_LENGTH;
        if (entitiesList.size() % PAGE_LENGTH != 0) count++;
        return count;
    }

    /**
     * Retrieves entities that belong to the specific page
     * @return List of entities that belong to the specific page
     */
    protected ArrayList<T> getEntitiesInCurrentPage() {
        ArrayList<T> entitiesInPage = new ArrayList<>(
                entitiesList.subList(pageNumber * PAGE_LENGTH,
                        Math.min(entitiesList.size(), (pageNumber + 1) * PAGE_LENGTH))
        );
        return entitiesInPage;
    }

    /**
     * Implements the logic of item clicking
     * @param item Specific item in the list
     */
    public void checkClick(T item){
        if (this.getSelectedEntities().contains(item)){
            selectedEntities.remove(item);
            TotalSelected--;
        }else{
            selectedEntities.add(item);
            TotalSelected++;
        }
        setDialogControlButtons();
    }
}