package com.somee.interestschat.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Alexander on 05.11.2016.
 */

/**
 * Class that implements database interaction logic
 */
public class ChatSqlLiteDBHelper extends SQLiteOpenHelper {
    // region Constants

    // Constants for database at whole
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ChatDroidDB";

    public static final String TABLE_NAME_USERS = "Users";

    // Constants for users table
    public static final String KEY_ID = "_id";
    //public static final String KEY_USERNAME = "name";
    public static final String KEY_EMAIL = "email";

    // endregion

    public ChatSqlLiteDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // region Creation and deleting

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_NAME_USERS
                + "(" + KEY_ID + " integer primary key, "
                //+ KEY_USERNAME + "text, "
                + KEY_EMAIL + " text"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exist " + DATABASE_NAME);
        onCreate(sqLiteDatabase);
    }

    // endregion
}
