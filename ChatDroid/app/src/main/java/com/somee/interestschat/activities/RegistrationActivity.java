package com.somee.interestschat.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.somee.interestschat.R;
import com.somee.interestschat.helpers.ChatSqlLiteDBHelper;
import com.somee.interestschat.fakes.Global;
import com.somee.interestschat.helpers.BasicDataService;
import com.somee.interestschat.helpers.SecurityHelper;
import com.somee.interestschat.models.User;

import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationActivity extends AppCompatActivity {

    private Button cmdRegistrationButton;
    private Button cmdLogin;
    private Button cmdCreateNewAccount;
    private Button cmdLoginIfHaveAccount;
    private EditText txtLogin;
    private EditText txtEmail;
    private ImageButton comboEmails;
    private EditText txtPassword;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private PopupMenu knownEmails;
    private static ArrayList<String> DBEmails = new ArrayList<>();

    /**
     * Calls on creation the activity
     * @param savedInstanceState Bundle object for restoring the state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        getViewItems();
        getKnownUsersToList();
        setViewItemsVisibility();
    }

    /**
     * Sets all layout elements
     */
    private void getViewItems() {
        this.cmdRegistrationButton = (Button) findViewById(R.id.cmdRegister);
        this.cmdLogin = (Button) findViewById(R.id.cmdLogin);
        this.cmdCreateNewAccount = (Button) findViewById(R.id.cmdCreateNewAccount);
        this.cmdLoginIfHaveAccount = (Button) findViewById(R.id.cmdToLoginIfHaveAccount);
        this.txtLogin = (EditText) findViewById(R.id.txtLogin);
        this.txtEmail = (EditText) findViewById(R.id.txtEmail);
        this.comboEmails = (ImageButton) findViewById(R.id.imgPopupEmails);
        this.txtPassword = (EditText) findViewById(R.id.txtPassword);
    }

    /**
     * Gets users from the database
     */
    private void getKnownUsersToList() {
        knownEmails = new PopupMenu(this, comboEmails);

        // get users from db
        ChatSqlLiteDBHelper dbHelper = new ChatSqlLiteDBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(dbHelper.TABLE_NAME_USERS, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            // get indexes of columns
            int idIndex = cursor.getColumnIndex(dbHelper.KEY_ID);
            int emailIndex = cursor.getColumnIndex(dbHelper.KEY_EMAIL);

            do {
                String email = cursor.getString(emailIndex);
                RegistrationActivity.DBEmails.add(email);

                knownEmails.getMenu().add(email);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        knownEmails.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                txtEmail.setText(item.getTitle().toString());
                return true;
            }
        });
    }

    /**
     * Sets some unused elements invisible
     */
    private void setViewItemsVisibility() {
        if (Global.isAuthorized()) {
            this.txtLogin.setVisibility(View.GONE);

            this.cmdRegistrationButton.setVisibility(View.GONE);
            this.cmdLoginIfHaveAccount.setVisibility(View.GONE);

            this.comboEmails.setVisibility(View.VISIBLE);
            this.cmdLogin.setVisibility(View.VISIBLE);
            this.cmdCreateNewAccount.setVisibility(View.VISIBLE);
        } else {
            this.txtLogin.setVisibility(View.VISIBLE);

            this.cmdLoginIfHaveAccount.setVisibility(View.VISIBLE);
            this.cmdRegistrationButton.setVisibility(View.VISIBLE);

            this.comboEmails.setVisibility(View.GONE);
            this.cmdLogin.setVisibility(View.GONE);
            this.cmdCreateNewAccount.setVisibility(View.GONE);
        }
    }

    /**
     * Registration of new user
     * @param view "Register me" button
     * @throws NoSuchAlgorithmException If MD5 is not implemented
     */
    public void onRegisterButtonClick(View view) throws NoSuchAlgorithmException {
        final String email = txtEmail.getText().toString();
        String login = txtLogin.getText().toString();
        String password = SecurityHelper.getMD5(txtPassword.getText().toString());

        if (!isInputValid())
            return;

        JSONObject us_json = null;
        try {
            String path = Global.ServerConnectionString + "/auth/register/" + email + "/" + login + "/" + password;
            String response = new BasicDataService().execute(path).get();
            us_json = new JSONObject(response);
        } catch (Exception e) {
        }

        try {
            User user = new User(us_json);
            if (user != null) {
                Intent intent = new Intent(getApplicationContext(), GroupsActivity.class);
                startActivity(intent);
                Global.CurrentUser = user.getId();
                Global.isAuthorized(true);
                this.finish();

                final Activity context = this;
                Thread handler = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // Save to DB
                        ChatSqlLiteDBHelper dbHelper = new ChatSqlLiteDBHelper(context);
                        SQLiteDatabase db = dbHelper.getWritableDatabase();
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(dbHelper.KEY_ID, DBEmails.size() + 1);
                        contentValues.put(dbHelper.KEY_EMAIL, email);
                        db.insert(dbHelper.TABLE_NAME_USERS, null, contentValues);
                    }
                });
                handler.run();
            } else {
                Toast.makeText(getApplicationContext(), "Error occured", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
        }
    }

    /**
     * Implementation of login logic
     * @param view "Login" button
     * @throws NoSuchAlgorithmException If MD5 is not implemented
     */
    public void onLoginButtonClick(View view) throws NoSuchAlgorithmException {
        String email = txtEmail.getText().toString();
        String password = SecurityHelper.getMD5(txtPassword.getText().toString());
        try {
            String path = Global.ServerConnectionString + "/auth/login/" + email + "/" + password;
            String response = new BasicDataService().execute(path).get().replace("\n", "");
            int userId = Integer.parseInt(response);
            if (userId != -1) {
                if (DBEmails.indexOf(email) == -1) {
                    ChatSqlLiteDBHelper dbHelper = new ChatSqlLiteDBHelper(this);
                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(dbHelper.KEY_ID, DBEmails.size() + 1);
                    contentValues.put(dbHelper.KEY_EMAIL, email);
                    db.insert(dbHelper.TABLE_NAME_USERS, null, contentValues);
                }

                Intent intent = new Intent(getApplicationContext(), GroupsActivity.class);
                startActivity(intent);
                Global.CurrentUser = userId;
                this.finish();
            } else {
                Toast.makeText(getApplicationContext(), "Invalid email or password", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
        }
    }

    /**
     * Prepares view for creating new account
     * @param view "Create new" button
     */
    public void onCreateNewAccount(View view) {
        Global.isAuthorized(false);

        getViewItems();
        setViewItemsVisibility();
    }

    /**
     * Prepares view for loggin
     * @param view "Have account" button
     */
    public void onLoginIfHaveAccount(View view) {
        Global.isAuthorized(true);

        getViewItems();
        setViewItemsVisibility();
    }

    /**
     * Validates user's input
     * @return True only if user's input is correct
     */
    private boolean isInputValid() {
        try {
            String email = txtEmail.getText().toString();
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(email);
            if (email.length() <= 3) throw new Exception("Email is too short");
            if (!matcher.matches()) throw new Exception("Email form is invalid");

            if (txtLogin.getText().length() <= 3) throw new Exception("Login is too short");

            if (txtPassword.getText().length() <= 3) throw new Exception("Password is too short");
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Shows users from the database
     * @param view Dropdown menu button
     */
    public void showKnownUsers(View view){
        knownEmails.show();
    }
}