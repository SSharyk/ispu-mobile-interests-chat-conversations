﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Dictionaries
{
    public class UserStatusDictionary
    {
        private static Dictionary<int, string> _dictionary;

        static UserStatusDictionary()
        {
            _dictionary = new Dictionary<int, string>();

            _dictionary.Add(1, "Online");
            _dictionary.Add(2, "Ofline");
            _dictionary.Add(3, "Turned off");
        }

        public string this[int i]
        {
            get { return _dictionary[i]; }
            set { _dictionary[i] = value; }
        }
    }
}