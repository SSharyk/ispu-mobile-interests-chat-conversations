﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Dictionaries
{
    public class MimeTypeDictionary
    {
        private static Dictionary<int, string> _dictionary;

        static MimeTypeDictionary()
        {
            _dictionary = new Dictionary<int, string>();

            _dictionary.Add(1, "Note");
            
            _dictionary.Add(2, "Txt");
            _dictionary.Add(3, "Pdf");
            
            _dictionary.Add(4, "Jpg");

            _dictionary.Add(5, "Mp3");
            _dictionary.Add(6, "Avi");

            _dictionary.Add(7, "Any");
        }

        public string this[int i]
        {
            get { return _dictionary[i]; }
            set { _dictionary[i] = value; }
        }
    }
}