﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Dictionaries
{
    public class InviteStatusDictionary
    {
        private static Dictionary<int, string> _dictionary;

        static InviteStatusDictionary()
        {
            _dictionary = new Dictionary<int, string>();
            _dictionary.Add(1, "Was sent");
            _dictionary.Add(2, "Was read");
            _dictionary.Add(3, "Accepted");
            _dictionary.Add(4, "Cancelled");
        }

        public string this[int i]
        {
            get { return _dictionary[i].PadLeft(2); }
            set { _dictionary[i] = value; }
        }

        public int this[string v]
        {
            get
            {
                foreach (int key in _dictionary.Keys)
                {
                    if (_dictionary[key] == v) return key;
                }
                return -1;
            }
        }
    }
}