﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Dictionaries
{
    public class ConversationGroupDictionary
    {
        public static Dictionary<int, string> Dictionary { get; private set; }

        static ConversationGroupDictionary()
        {
            Dictionary = new Dictionary<int, string>();
            Dictionary.Add(1, "Music");
            Dictionary.Add(2, "Video");
            Dictionary.Add(3, "Books");
        }

        public string this[int i]
        {
            get { return Dictionary[i]; }
            set { Dictionary[i] = value; }
        }
    }
}