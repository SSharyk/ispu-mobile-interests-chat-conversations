﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Dictionaries
{
    public class VoteStatusDictionary
    {
        private static Dictionary<int, string> _dictionary;

        static VoteStatusDictionary()
        {
            _dictionary = new Dictionary<int, string>();

            _dictionary.Add(1, "+1");
            _dictionary.Add(2, "0");
            _dictionary.Add(3, "-1");
        }

        public string this[int i]
        {
            get { return _dictionary[i].PadLeft(2); }
            set { _dictionary[i] = value; }
        }
    }
}