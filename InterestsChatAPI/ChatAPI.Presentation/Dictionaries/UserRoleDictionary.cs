﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Dictionaries
{
    public class UserRoleDictionary
    {
        private static Dictionary<int, string> _dictionary;

        static UserRoleDictionary()
        {
            _dictionary = new Dictionary<int, string>();

            _dictionary.Add(1, "User");
            _dictionary.Add(2, "Admin");
        }

        public string this[int i]
        {
            get { return _dictionary[i]; }
            set { _dictionary[i] = value; }
        }
    }
}