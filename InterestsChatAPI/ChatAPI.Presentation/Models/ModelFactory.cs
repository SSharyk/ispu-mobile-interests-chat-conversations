﻿using ChatAPI.Presentation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Models
{
    public class ModelFactory
    {
        #region Create/Parse User

        public UserModel Create(User user)
        {
            if (user != null)
            {
                return new UserModel()
                {
                    Id = user.Id,
                    Login = user.Login,
                    Email = user.Email,
                    RoleId = user.RoleId
                };
            }
            else
            {
                return null;
            }
        }

        public User Parse(UserModel model)
        {
            if (model != null)
            {
                return new User()
                {
                    Id = model.Id,
                    Login = model.Login,                 
                    Email = model.Email,                    
                    RoleId = model.RoleId
                };
            }
            else
            {
                return null;
            }
        }

        public ICollection<UserModel> CreateListOfUsers(ICollection<User> allUsers)
        {
            if (allUsers != null)
            {
                var usersModelList = new List<UserModel>();
                foreach (User user in allUsers)
                {
                    usersModelList.Add(Create(user));
                }
                return usersModelList;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}