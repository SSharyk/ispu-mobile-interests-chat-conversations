﻿using System.Collections.Generic;
using System;

namespace ChatAPI.Presentation.Entities
{
    [Serializable]
    public class User
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public int RoleId { get; set; }

        public int StatusId { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1}, {2}, {3}\r\n", Id, Login, Email, Password);
        }
    }
}