﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Entities
{
    public class Friendship
    {
        public int FirstUserId { get; set; }
        public int SecondUserId { get; set; }
    }
}