﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Entities
{
    public class BoardItem
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ConversationId { get; set; }
        public int MimeId { get; set; }
        public string Title { get; set; }
        public DateTime LastUpdateDateTime { get; set; }
    }

    public enum MimeType
    {
        Note,

        Txt,
        Pdf,

        Jpg,

        Mp3,
        Avi,

        Any
    }
}