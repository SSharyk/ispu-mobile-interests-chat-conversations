﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Entities
{
    public class Ownership
    {
        public int ConversationId { get; set; }
        public int BoardItemId { get; set; }
    }
}