﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Entities
{
    public class Conversation
    {
        public int Id { get; set; }

        public int GroupId { get; set; }

        public string Name { get; set; }

        public string Tags { get; set; }

        public int PrivacyId { get; set; }

        public int Votes { get; set; }

        public int LastMessageId { get; set; }
        public string LastMessage { get; set; }
    }
}