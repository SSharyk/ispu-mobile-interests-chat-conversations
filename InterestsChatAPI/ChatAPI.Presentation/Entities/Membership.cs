﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Entities
{
    public class Membership
    {
        public int UserId { get; set; }
        public int ConversationId { get; set; }
        public int UserStatusId { get; set; }
        public int VoteStatusId { get; set; }
    }
}