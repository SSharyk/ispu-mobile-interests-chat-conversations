﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Entities
{
    public class Notification
    {
        public string Message { get; set; }
        public int EntityId { get; set; }
    }
}