﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Entities
{
    public class Invite
    {
        public int Id { get; set; }
        public int FromUserId { get; set; }
        public string FromUserName { get; set; }
        public int ToUserId { get; set; }
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public int InviteStatusId { get; set; }
        public int InviteReasonId { get; set; }
        public DateTime SendDate { get; set; }
    }
}