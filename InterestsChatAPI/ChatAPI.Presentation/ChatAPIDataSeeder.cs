﻿using ChatAPI.Presentation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChatAPI.Presentation
{
    class ChatAPIDataSeeder
    {
        private readonly ChatAPIContext _context;

        public ChatAPIDataSeeder(ChatAPIContext context)
        {
            this._context = context;
        }

        public void Seed()
        {
            /// TODO: check via references and not entities
            //if (this._context.UserRoles.Any())
            //{
            //    return;
            //}

            //_context.UserRoles.AddRange(UserRolesSeedData);
            //_context.UserStatuses.AddRange(UserStatusesSeedData);

            _context.Users.AddRange(UsersSeedData);

            this._context.SaveChanges();
        }

        //#region References

        //private static readonly IList<RefUserRole> UserRolesSeedData =
        //    //Enum.GetNames(typeof(UserRoleEnum)).Select((ur, i) => new UserRole { Id = i, Name = ur }).ToList();
        //    new List<RefUserRole>()
        //    {
        //        new RefUserRole{Id=1, Name=UserRoleEnum.Admin.ToString()},
        //        new RefUserRole{Id=2, Name=UserRoleEnum.User.ToString()}
        //    };

        //private static readonly IList<RefUserStatus> UserStatusesSeedData =
        //    //Enum.GetNames(typeof(UserStatusEnum)).Select((us, i) => new UserStatus { Id = i, Name = us }).ToList();
        //    new List<RefUserStatus>()
        //    {
        //        new RefUserStatus{Id=1, Name=UserStatusEnum.Offline.ToString()},
        //        new RefUserStatus{Id=2, Name=UserStatusEnum.Online.ToString()},
        //        new RefUserStatus{Id=3, Name=UserStatusEnum.TurnedOff.ToString()},
        //    };

        //#endregion

        #region Entities

        private static readonly IList<User> UsersSeedData = new List<User>
        {
            new User{Id=1, Email="testUser@email.loc", Login="testUser", Password=Utilities.SecurityHelper.Hash("password")},
        };

        #endregion
    }
}