﻿using ChatAPI.Presentation.Entities;
using ChatAPI.Presentation.EntitiesConfiguration;
using ChatAPI.Presentation.Migrations;
using System.Data.Entity;

namespace ChatAPI.Presentation
{
    public class ChatAPIContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        //public DbSet<RefUserRole> UserRoles { get; set; }
        //public DbSet<RefUserStatus> UserStatuses { get; set; }

        public ChatAPIContext() :
            base()
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ChatAPIContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Configurations.Add(new UserRoleConfiguration());
            //modelBuilder.Configurations.Add(new UserStatusConfiguration());

            modelBuilder.Configurations.Add(new UserConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}