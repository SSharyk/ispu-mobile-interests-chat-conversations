﻿using BoardItem = ChatAPI.Presentation.Entities.BoardItem;
using Mime = ChatAPI.Presentation.Entities.MimeType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Repositories
{
    public static class BoardRepository
    {
        private static List<BoardItem> Entity;

        static BoardRepository()
        {
            Entity = new List<BoardItem>();
            Entity.Add(new BoardItem() { Id = 1, ConversationId = 1, MimeId = (int)Mime.Note, Title = "This is fake note in conv. 1.1 by user 1", UserId = 1 });
            Entity.Add(new BoardItem() { Id = 2, ConversationId = 1, MimeId = (int)Mime.Note, Title = "This is one more fake note in conv. 1.1 by user 2", UserId = 2 });
            Entity.Add(new BoardItem() { Id = 3, ConversationId = 2, MimeId = (int)Mime.Note, Title = "In conv. _ID_=2 by user 1", UserId = 1 });
            Entity.Add(new BoardItem() { Id = 4, ConversationId = 3, MimeId = (int)Mime.Note, Title = "In _ID_=3 of 2", UserId = 2 });
            Entity.Add(new BoardItem() { Id = 5, ConversationId = 4, MimeId = (int)Mime.Note, Title = "In _ID_=4 of 3", UserId = 3 });
            Entity.Add(new BoardItem() { Id = 6, ConversationId = 5, MimeId = (int)Mime.Note, Title = "Of user 1 (in _6_) again. Last, I hope", UserId = 1 });
        }

        #region	Getting

        public static DalOperationStatus<BoardItem> Get(int itemId)
        {
            DalOperationStatus<BoardItem> os = new DalOperationStatus<BoardItem>();
            try
            {
                os.Entity = Entity.SingleOrDefault(bi => bi.Id == itemId);
                os.Message = "Note" + itemId + " received sucessfully";
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception e)
            {
                os.Exception = e;
                os.Message = "Cannot receive note #" + itemId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<List<BoardItem>> GetList(int conversationId)
        {
            DalOperationStatus<List<BoardItem>> os = new DalOperationStatus<List<BoardItem>>();
            try
            {
                os.Entity = Entity.Where(bi => bi.ConversationId == conversationId).ToList();
                os.Message = "List of notes received sucessfully";
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception e)
            {
                os.Entity = new List<BoardItem>();
                os.Exception = e;
                os.Message = "Cannot receive notes of conversation #" + conversationId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<List<BoardItem>> GetListOfUserNotes(int userId)
        {
            DalOperationStatus<List<BoardItem>> os = new DalOperationStatus<List<BoardItem>>();
            try
            {
                os.Entity = Entity.Where(bi => bi.UserId == userId).ToList();
                os.Message = "List of user's notes received sucessfully";
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception e)
            {
                os.Entity = new List<BoardItem>();
                os.Exception = e;
                os.Message = "Cannot receive notes of user #" + userId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        #endregion

        #region CRUD

        public static DalOperationStatus<BoardItem> CreateNote(int conversationId, int userId, string text)
        {
            var os = new DalOperationStatus<BoardItem>();
            try
            {
                BoardItem item = new BoardItem()
                {
                    Id = Entity.Count + 1,
                    MimeId = (int)Mime.Note,
                    ConversationId = conversationId,
                    UserId = userId,
                    Title = text.Replace('_', ' '),
                    LastUpdateDateTime = DateTime.Now
                };

                Entity.Add(item);

                os.Entity = item;
                os.Message = "Note created: #" + item.Id;
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.Exception = e;
                os.Message = "Error occured during creating the note";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<BoardItem> UpdateNote(int noteId, string text)
        {
            var os = new DalOperationStatus<BoardItem>();
            try
            {
                var itemOrigIndex = Entity.FindIndex(bi => bi.Id == noteId);
                if (itemOrigIndex == -1)
                {
                    os.Entity = null;
                    os.Exception = new NullReferenceException("Has no notes with #" + noteId);
                    os.Message = "No note #" + noteId;
                    os.StatusCode = DalOperationStatusCode.Error;
                }
                else
                {
                    Entity[itemOrigIndex].Title = text.Replace('_', ' ');

                    os.Entity = Entity[itemOrigIndex];
                    os.Message = "Note updated: #" + noteId;
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                }
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.Exception = e;
                os.Message = "Error occured during updating the note #" + noteId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<int> DeleteNote(int noteId)
        {
            var os = new DalOperationStatus<int>();
            try
            {
                var itemOrigIndex = Entity.FindIndex(bi => bi.Id == noteId);
                if (itemOrigIndex == -1)
                {
                    os.Entity = 0;
                    os.Exception = new NullReferenceException("Has no notes with #" + noteId);
                    os.Message = "No note #" + noteId;
                    os.StatusCode = DalOperationStatusCode.Error;
                }
                else
                {
                    Entity.RemoveAt(itemOrigIndex);
                    os.Entity = noteId;
                    os.Message = "Note deleted: #" + noteId;
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                }
            }
            catch (Exception e)
            {
                os.Entity = 0;
                os.Exception = e;
                os.Message = "Error occured during deleting the note #" + noteId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        #endregion
    }
}