﻿using ChatAPI.Presentation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Repositories
{
    public static class NotificationRepository
    {
        public static DalOperationStatus<List<Notification>> GetList(int userId)
        {
            DalOperationStatus<List<Notification>> os = new DalOperationStatus<List<Notification>>();
            try
            {
                os.Entity = new List<Notification>();
                DateTime now = DateTime.Now;

                var conversations = MembershipRepository.GetConversationsOfUser(userId).Entity;
                foreach (var conv in conversations)
                {
                    var messages = MessagesRepository.GetConversationMessages(conv.Id).Entity;
                    int count = messages.Count(m => now - m.PostDateTime <= new TimeSpan(0, 0, 30));
                    if (count > 0)
                        os.Entity.Add(new Notification() { EntityId = conv.Id, Message = "Новых сообщений: " + count + " в беседе: " + conv.Name });
                }
            }
            catch (Exception e)
            {
                os.Entity = new List<Notification>();
                os.Exception = e;
                os.Message = "Cannot receive notifications for user" + userId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }
    }
}