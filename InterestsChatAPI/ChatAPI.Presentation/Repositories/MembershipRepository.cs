﻿using ChatAPI.Presentation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Repositories
{
    public class MembershipRepository
    {
        private static List<Membership> Membership;

        static MembershipRepository()
        {
            Membership = new List<Membership>();
            Membership.Add(new Membership() { ConversationId = 1, UserId = 1, UserStatusId = 1, VoteStatusId = 2});
            Membership.Add(new Membership() { ConversationId = 2, UserId = 1, UserStatusId = 1, VoteStatusId = 1 });
            Membership.Add(new Membership() { ConversationId = 5, UserId = 1, UserStatusId = 1, VoteStatusId = 3 });
            Membership.Add(new Membership() { ConversationId = 2, UserId = 2, UserStatusId = 2, VoteStatusId = 2 });
            Membership.Add(new Membership() { ConversationId = 5, UserId = 3, UserStatusId = 2, VoteStatusId = 2 });
        }

        public static DalOperationStatus<List<Conversation>> GetConversationsOfUser(int userId)
        {
            var os = new DalOperationStatus<List<Conversation>>();
            os.Entity = new List<Conversation>();
            try
            {
                var membership = Membership.Where(m => m.UserId == userId).Select(m=>m.ConversationId).ToList();

                foreach (int usId in membership)
                {
                    os.Entity.Add(ConversationRepository.Get(usId).Entity);
                }
                os.Message = "Conversations are found successfully.";
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception ex)
            {
                os.Message = "Unable to get user's Conversations.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<int> GetCommonConversationsCount(int first, int second)
        {
            var os = new DalOperationStatus<int>();
            try
            {
                var convsOfFirst = GetConversationsOfUser(first).Entity;
                var convsOfSecond = GetConversationsOfUser(second).Entity;

                os.Entity = convsOfFirst.Intersect(convsOfSecond).Count();
                os.Message = "Count common conversations successfully";
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception e)
            {
                os.Entity = -1;
                os.Exception = e;
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = string.Format("Cannot count common conversations for {0} and {1}", first, second);
            }
            return os;
        }

        public static DalOperationStatus<bool> Add(int userId, int conversationId, int userStatusId = 1)
        {
            DalOperationStatus<bool> os = new DalOperationStatus<bool>();
            try
            {
                os.Entity = !Membership.Any(memb => memb.UserId == userId &&
                    memb.ConversationId == conversationId);
                if (os.Entity)
                {
                    Membership.Add(new Membership() { UserId = userId, ConversationId = conversationId, UserStatusId = userStatusId });
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Membership added for User#" + userId + " and conversation #" + conversationId;
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.NoChanges;
                    os.Message = "User " + userId + " is member of conversation " + conversationId + " already";
                }
            }
            catch (Exception e)
            {
                os.Entity = false;
                os.Exception = e;
                os.Message = "Cannot add membership for User#" + userId + " and conversation #" + conversationId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<bool> Remove(int userId, int conversationId)
        {
            DalOperationStatus<bool> os = new DalOperationStatus<bool>();
            try
            {
                int count = Membership.RemoveAll(x => x.UserId == userId && x.ConversationId == conversationId);
                os.Entity = count > 0;
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Membership deleted for User#" + userId + " and conversation #" + conversationId;
            }
            catch (Exception e)
            {
                os.Entity = false;
                os.Exception = e;
                os.Message = "Cannot delete membership for User#" + userId + " and conversation #" + conversationId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<bool> RemoveAllUsers(int conversationId)
        {
            DalOperationStatus<bool> os = new DalOperationStatus<bool>();
            try
            {
                int count = Membership.RemoveAll(x => x.ConversationId == conversationId);
                os.Entity = true;
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Membership deleted for all users in conversation #" + conversationId;
            }
            catch (Exception e)
            {
                os.Entity = false;
                os.Exception = e;
                os.Message = "Cannot delete membership for conversation #" + conversationId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }
    }
}