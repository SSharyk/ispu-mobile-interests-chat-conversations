﻿using ChatAPI.Presentation.Dictionaries;
using ChatAPI.Presentation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Repositories
{
    public class FriendshipRepository
    {
        private static List<Friendship> Friendship;

        static FriendshipRepository()
        {
            Friendship = new List<Friendship>();
            Friendship.Add(new Friendship() { FirstUserId = 1, SecondUserId = 2 });
            Friendship.Add(new Friendship() { FirstUserId = 3, SecondUserId = 1 });
        }

        public static DalOperationStatus<List<User>> GetFriendsOfUser(int userId)
        {
            var os = new DalOperationStatus<List<User>>();
            os.Entity = new List<User>();
            try
            {
                var friendships = Friendship.Where(fr => fr.FirstUserId == userId).Select(fr => fr.SecondUserId).ToList();
                friendships.AddRange(Friendship.Where(fr => fr.SecondUserId == userId).Select(fr => fr.FirstUserId).ToList());

                foreach (int usId in friendships)
                {
                    os.Entity.Add(UserRepository.Get(usId).Entity);
                }
                os.Message = "Friends are found successfully.";
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception ex)
            {
                os.Message = "Unable to get user's friends.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<List<User>> GetNotFriendsOfUser(int userId)
        {
            var os = new DalOperationStatus<List<User>>();
            os.Entity = new List<User>();
            try
            {
                var allUsers = UserRepository.GetList();
                var friends = FriendshipRepository.GetFriendsOfUser(userId);
                var haveInvitesUsers = InviteRepository.GetFriendship(userId);             

                os.Entity = allUsers.Entity
                    .Except(friends.Entity).Except(haveInvitesUsers.Entity)
                    .Where(us => us.Id != userId).ToList();
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "List of not friends received";
            }
            catch (Exception ex)
            {
                os.Message = "Unable to get user's not friends.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<bool> Add(int userId1, int userId2)
        {
            DalOperationStatus<bool> os = new DalOperationStatus<bool>();
            try
            {
                os.Entity = !Friendship.Any(fr => fr.FirstUserId == userId1 && fr.SecondUserId == userId2 ||
                                                  fr.FirstUserId == userId2 && fr.SecondUserId == userId1);
                if (os.Entity)
                {
                    Friendship.Add(new Friendship() { FirstUserId = userId1, SecondUserId = userId2 });
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Friendship added for Users #" + userId1 + " and #" + userId2;
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.NoChanges;
                    os.Message = "Users #" + userId1 + " and #" + userId2 + " are friends already";
                }
            }
            catch (Exception e)
            {
                os.Entity = false;
                os.Exception = e;
                os.Message = "Cannot add friendship for Users #" + userId1 + " and #" + userId2;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }
    }
}