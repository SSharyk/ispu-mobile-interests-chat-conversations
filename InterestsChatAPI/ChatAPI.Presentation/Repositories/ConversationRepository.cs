﻿using ChatAPI.Presentation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Repositories
{
    public class ConversationRepository
    {
        private static List<Conversation> Entity;

        static ConversationRepository()
        {
            Entity = new List<Conversation>();
            Entity.Add(new Conversation() { Id = 1, GroupId = 1, Name = "Conv.1.1", PrivacyId = 1, LastMessageId = 3, Tags = "Films, About us", Votes = 3 });
            Entity.Add(new Conversation() { Id = 2, GroupId = 2, Name = "Conv.2.1", PrivacyId = 2, LastMessageId = 8, Tags = "Music, ABBA, Dancing Queen", Votes = 4 });
            Entity.Add(new Conversation() { Id = 3, GroupId = 1, Name = "Conv.1.2", PrivacyId = 2, LastMessageId = 11, Tags = "Films, Some film", Votes = 0 });
            Entity.Add(new Conversation() { Id = 4, GroupId = 1, Name = "Conv.1.3", PrivacyId = 2, LastMessageId = 12, Tags = "Films, Othe film", Votes = -3 });
            Entity.Add(new Conversation() { Id = 5, GroupId = 2, Name = "Conv.2.2", PrivacyId = 1, LastMessageId = 13, Tags = "Music, ABBA, Summer Night City, In the sun...", Votes = 4 });

            Entity.Select(r => r.LastMessage = MessagesRepository.GetMessage(r.LastMessageId).Entity.Text).ToList();
        }

        public static DalOperationStatus<Conversation> Get(int id)
        {
            var os = new DalOperationStatus<Conversation>();
            try
            {
                os.Entity = ConversationRepository.Entity.Where(c => c.Id == id).Single();
                //os.Entity = _context.Users.Where(us => us.Id == userId).Single();
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Conversation successfully received.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get conversation with the specific id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<List<Conversation>> GetList()
        {
            var os = new DalOperationStatus<List<Conversation>>();
            try
            {
                os.Entity = new List<Conversation>(ConversationRepository.Entity);
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Conversations successfully received.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get conversations.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<List<Conversation>> GetListForSelection(int userId)
        {
            var os = GetList();
            try
            {
                var member = MembershipRepository.GetConversationsOfUser(userId);
                os.Entity = os.Entity.Except(member.Entity).ToList();

                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Conversations successfully received.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get conversations.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<Conversation> Create(Conversation conversation)
        {
            var os = new DalOperationStatus<Conversation>();
            try
            {
                if (conversation.Id == 0 && !Entity.Any(c => c.Name == conversation.Name))
                {
                    conversation.Id = Entity.Count + 1;
                    Entity.Add(conversation);
                    os.Entity = conversation;
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Conversations successfully received.";
                }
                else
                {
                    os.Entity = null;
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "We have such conversation already.";
                }
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Entity = null;
                os.Message = "Unable to create conversations.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<Conversation> Create(string name, int privacyId, int categoryId, int userId)
        {
            var os = new DalOperationStatus<Conversation>();
            try
            {
                Message mess = MessagesRepository.Create(Entity.Count + 1, userId).Entity;
                Conversation conversation = new Conversation()
                {
                    Id = 0,
                    GroupId = categoryId,
                    Name = name.Replace('_', ' '),
                    PrivacyId = privacyId,
                    LastMessageId = mess.Id,
                    LastMessage = mess.Text,
                    Tags = ""
                };
                os = ConversationRepository.Create(conversation);
                if (os.StatusCode == DalOperationStatusCode.OperationSuccessful)
                    MembershipRepository.Add(userId, os.Entity.Id);
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to create conversations.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<bool> Remove(int id)
        {
            var os = new DalOperationStatus<bool>();
            try
            {
                var members = MembershipRepository.RemoveAllUsers(id);
                int count = Entity.RemoveAll(x => x.Id == id);
                os.Entity = count > 0;
                //os.Entity = _context.Users.Where(us => us.Id == userId).Single();
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Conversation successfully removed.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to remove conversation with the specific id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<List<string>> GetCategoriesList()
        {
            DalOperationStatus<List<string>> os = new DalOperationStatus<List<string>>();
            try
            {
                Dictionary<int, string> dict = Dictionaries.ConversationGroupDictionary.Dictionary;
                os.Entity = new List<string>(dict.Values);
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "ConversationGroupDictionary used successfully";
            }
            catch (Exception e)
            {
                os.Entity = new List<string>();
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "Error in ConversationGroupDictionary";
                os.Exception = e;
            }
            return os;
        }
    }
}