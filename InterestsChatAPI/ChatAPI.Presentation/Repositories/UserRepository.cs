﻿using ChatAPI.Presentation.Entities;
using ChatAPI.Presentation.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ChatAPI.Presentation.Repositories
{
    public class UserRepository : IEnumerable<User> //, IRepository<User>
    {
        private ChatAPIContext _context;
        public static List<User> Entity { get; set; }

        public UserRepository(ChatAPIContext ctx)
        {
            this._context = ctx;
        }
        static UserRepository()
        {
            Entity = new List<User>();
            Entity.Add(new User() { Id = 1, Email = "mail1@mail.mail", Login = "login1", Password = "5f4dcc3b5aa765d61d8327deb882cf99", RoleId = 1, StatusId = 2 });
            Entity.Add(new User() { Id = 2, Email = "mail2@mail.mail", Login = "login2", Password = "5f4dcc3b5aa765d61d8327deb882cf99", RoleId = 1, StatusId = 1 });
            Entity.Add(new User() { Id = 3, Email = "mail3@mail.mail", Login = "login3", Password = "5f4dcc3b5aa765d61d8327deb882cf99", RoleId = 2, StatusId = 1 });
        }

        public IEnumerator<User> GetEnumerator()
        {
            return Entity.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Entity.GetEnumerator();
        }

        public static DalOperationStatus<List<User>> GetList()
        {
            var os = new DalOperationStatus<List<User>>();
            try
            {
                //os.Entity = _context.Users.ToList();
                os.Entity = new List<User>(UserRepository.Entity);
                var usersCount = os.Entity.ToList<User>().Count;
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "List of users successfully received.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Entity = new List<User>() { new User() { Id = -1, Email = ex.StackTrace, Password = "---", Login = ex.Message } };
                os.Message = "Unable to get users list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<User> Get(int userId)
        {
            var os = new DalOperationStatus<User>();
            try
            {
                os.Entity = UserRepository.Entity.Where(us => us.Id == userId).Single();
                //os.Entity = _context.Users.Where(us => us.Id == userId).Single();
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "List of users successfully received.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get user with the specific id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<int> Create(User user)
        {
            var os = new DalOperationStatus<int>();
            try
            {
                Entity.Add(user);
                //_context.Users.Add(user);
                //if (_context.SaveChanges() > 0)
                {
                    os.Entity = user.Id;
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "User successfully added.";
                }
                //else
                //{
                //    os.StatusCode = DalOperationStatusCode.Error;
                //    os.Message = "User not added.";
                //}
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "User add error occured.");
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "User add error occured.";
                os.Exception = ex;
            }
            return os;
        }

        public static DalOperationStatus<int> Update(User user)
        {
            var os = new DalOperationStatus<int>();
            //var userOriginal = _context.Users.Find(user.Id);
            var userOriginal = Entity.Single(us => us.Id == user.Id);
            if (userOriginal != null)
            {
                try
                {
                    /// TODO: check if update the specific list item occurs saving changes in the list
                    userOriginal.Login = user.Login;
                    userOriginal.Email = user.Email;
                    userOriginal.Password = user.Password;
                    userOriginal.RoleId = user.RoleId;
                    userOriginal.StatusId = user.StatusId;
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User wasn't updated.";
                    os.Exception = ex;
                    //GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a user.");
                    os.Entity = -1;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "User not found.";
                os.Entity = -1;
            }
            return os;
        }

        public static DalOperationStatus<int> Delete(User item)
        {
            throw new NotImplementedException();
        }

        public static DalOperationStatus<int> Delete(int userId)
        {
            var os = new DalOperationStatus<int>();
            //var user = _context.Users.Find(userId);
            var user = Entity.Single(us => us.Id == userId);
            if (user != null)
            {
                try
                {
                    //_context.SaveChanges();
                    Entity.Remove(user);
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "User deleted.";
                    os.Entity = userId;
                }
                catch (Exception ex)
                {
                    //GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete an user.");
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User not deleted.";
                    os.Exception = ex;
                    os.Entity = -1;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Entity = -1;
                os.Message = "User not found.";
            }
            return os;
        }

        #region For auth

        public static DalOperationStatus<User> Create(string email, string login, string password)
        {
            DalOperationStatus<User> os = new DalOperationStatus<User>();
            try
            {
                var x = UserRepository.Get(email);
                if (x.Entity != null)
                    throw new AuthorizationException(ExceptionReason.SuchUserEmailRegisteredAlready);
                User user = new User()
                {
                    Id = Entity.Count + 1,
                    Email = email,
                    Login = login,
                    Password = password,
                    RoleId = 2,
                    StatusId = 1
                };

                Entity.Add(user);
                os.Entity = user;
                os.Message = "New user registered successfully";
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.Exception = e;
                os.Message = (e is AuthorizationException) ? e.Message : "Cannot register new user";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<User> Get(string email)
        {
            var os = new DalOperationStatus<User>();
            try
            {
                os.Entity = UserRepository.Entity.SingleOrDefault(us => us.Email == email);
                //os.Entity = _context.Users.Where(us => us.Id == userId).Single();
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "User was received successfully.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get user with the specific email.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<User> Get(string email, string password)
        {
            var os = new DalOperationStatus<User>();
            try
            {
                os.Entity = UserRepository.Entity.SingleOrDefault(us => us.Email == email
                                                                     && us.Password == password);
                //os.Entity = _context.Users.Where(us => us.Id == userId).Single();
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "User was received successfully.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get user with the specific email.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        #endregion

        public void Dispose()
        {
        }
    }
}