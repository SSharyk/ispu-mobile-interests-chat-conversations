﻿using ChatAPI.Presentation.Dictionaries;
using ChatAPI.Presentation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Repositories
{
    public static class InviteRepository
    {
        public static List<Invite> Entity { get; private set; }

        static InviteRepository()
        {
            Entity = new List<Invite>();
            Entity.Add(new Invite() { Id = 1, FromUserId = 1, ToUserId = 2, InviteReasonId = 1, InviteStatusId = 3, EntityId = -1, SendDate = DateTime.Now });
            Entity.Add(new Invite() { Id = 2, FromUserId = 3, ToUserId = 2, InviteReasonId = 2, InviteStatusId = 1, EntityId = 1, SendDate = DateTime.Now });
            Entity.Add(new Invite() { Id = 3, FromUserId = 1, ToUserId = 3, InviteReasonId = 1, InviteStatusId = 3, EntityId = -1, SendDate = DateTime.Now });
        }

        public static DalOperationStatus<Invite> Send(int reason, int from, int to, int entity = -1)
        {
            DalOperationStatus<Invite> os = new DalOperationStatus<Invite>();
            try
            {
                Invite invite = new Invite()
                {
                    Id = Entity.Count + 1,
                    FromUserId = from,
                    ToUserId = to,
                    InviteReasonId = reason,
                    InviteStatusId = 1,
                    SendDate = DateTime.Now,
                    EntityId = entity
                };

                Entity.Add(invite);
                os.Entity = invite;
                os.Message = "Invite is sent successfully";
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.Exception = e;
                os.Message = string.Format("Cannot send an from {0} to {1} of reason {2}", from, to, reason);
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<Invite> SetStatus(int inviteId, int statusId)
        {
            DalOperationStatus<Invite> os = new DalOperationStatus<Invite>();
            try
            {
                /// TODO: check if entity really changed in list
                Invite invite = Entity.Single(inv => inv.Id == inviteId);
                invite.InviteStatusId = statusId;

                os.Entity = invite;
                os.Message = "Invite's status is changed successfully to " + (new InviteStatusDictionary())[statusId];
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.Exception = e;
                os.Message = "Cannot change invite #" + inviteId + " status";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<Invite> Get(int inviteId)
        {
            DalOperationStatus<Invite> os = new DalOperationStatus<Invite>();
            try
            {
                os.Entity = Entity.SingleOrDefault(inv => inv.Id == inviteId);
                os.Entity.EntityName = ConversationRepository.Get(os.Entity.EntityId).Entity.Name;
                os.Entity.FromUserName = UserRepository.Get(os.Entity.FromUserId).Entity.Login;
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.Exception = e;
                os.Message = "Cannot get invite #" + inviteId;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }
        
        public static DalOperationStatus<List<Invite>> GetList(int userId, bool isUnread)
        {
            DalOperationStatus<List<Invite>> os = new DalOperationStatus<List<Invite>>();
            try
            {
                os.Entity = (isUnread)
                    ? Entity.Where(inv => inv.ToUserId == userId && inv.InviteStatusId == 1).ToList()     // unread
                    : Entity.Where(inv => inv.FromUserId == userId).ToList();   // sent
                foreach (var invite in os.Entity)
                {                 
                    invite.FromUserName = UserRepository.Get(invite.FromUserId).Entity.Login;
                    if (invite.EntityId != -1)
                        invite.EntityName = ConversationRepository.Get(invite.EntityId).Entity.Name;
                }
                os.Message = string.Format("List of invites user #{0} {1} was received",
                    userId, (isUnread) ? "did not read" : "sent");
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.Exception = e;
                os.Message = "Cannot get list of user #" + userId + " invites";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<List<User>> GetFriendship(int userId)
        {
            var os = new DalOperationStatus<List<User>>();
            try
            {
                //os.Entity = _context.Users.ToList();
                os.Entity = new List<User>();
                var invites = Entity.Where(inv => (inv.FromUserId == userId || inv.ToUserId == userId)
                    && inv.InviteReasonId == (new InviteReasonDictionary())["Friendship"]);
                foreach (Invite inv in invites)
                {
                    User user = UserRepository.Get((inv.FromUserId == userId) ? inv.ToUserId : inv.FromUserId).Entity;
                    os.Entity.Add(user);
                }
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "List of users successfully received.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Entity = new List<User>() { new User() { Id = -1, Email = ex.StackTrace, Password = "---", Login = ex.Message } };
                os.Message = "Unable to get users list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

    }

}