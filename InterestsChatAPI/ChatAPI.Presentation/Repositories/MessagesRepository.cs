﻿using ChatAPI.Presentation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Repositories
{
    public class MessagesRepository
    {
        private static List<Message> Entity;

        static MessagesRepository()
        {
            Entity = new List<Message>();

            Entity.Add(new Message() { Id = 1, ConversationId = 1, UserId = 1, Text = "message 1.1", PostDateTime = DateTime.Now });
            Entity.Add(new Message() { Id = 2, ConversationId = 1, UserId = 2, Text = "message 1.2", PostDateTime = DateTime.Now });
            Entity.Add(new Message() { Id = 3, ConversationId = 1, UserId = 1, Text = "message 1.3", PostDateTime = DateTime.Now });

            Entity.Add(new Message() { Id = 4, ConversationId = 2, UserId = 1, Text = "message 2.1", PostDateTime = DateTime.Now });
            Entity.Add(new Message() { Id = 5, ConversationId = 2, UserId = 2, Text = "message 2.2", PostDateTime = DateTime.Now });
            Entity.Add(new Message() { Id = 6, ConversationId = 2, UserId = 1, Text = "message 2.3", PostDateTime = DateTime.Now });
            Entity.Add(new Message() { Id = 7, ConversationId = 2, UserId = 1, Text = "message 2.4", PostDateTime = DateTime.Now });
            Entity.Add(new Message() { Id = 8, ConversationId = 2, UserId = 1, Text = "message 2.5", PostDateTime = DateTime.Now });

            Entity.Add(new Message() { Id = 9, ConversationId = 3, UserId = 1, Text = "message 3.1", PostDateTime = DateTime.Now });
            Entity.Add(new Message() { Id = 10, ConversationId = 3, UserId = 2, Text = "message 3.2", PostDateTime = DateTime.Now });
            Entity.Add(new Message() { Id = 11, ConversationId = 3, UserId = 1, Text = "message 3.3", PostDateTime = DateTime.Now });

            Entity.Add(new Message() { Id = 12, ConversationId = 4, UserId = 2, Text = "message 4.0", PostDateTime = DateTime.Now });
            
            Entity.Add(new Message() { Id = 13, ConversationId = 5, UserId = 1, Text = "message 5.0", PostDateTime = DateTime.Now });
        }

        #region Getting

        public static DalOperationStatus<List<Message>> GetConversationMessages(int conversationId)
        {
            DalOperationStatus<List<Message>> os = new DalOperationStatus<List<Message>>();
            try
            {
                os.Entity = new List<Message>(Entity.Where(mes => mes.ConversationId == conversationId));
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Messages received successfully";
            }
            catch (Exception e)
            {
                os.Entity = new List<Message>();
                os.StatusCode = DalOperationStatusCode.Error;
                os.Exception = e;
                os.Message = "Cannot receive messages";
            }
            return os;
        }

        public static DalOperationStatus<List<Message>> GetConversationMessages(int conversationId, int userId)
        {
            DalOperationStatus<List<Message>> os = new DalOperationStatus<List<Message>>();
            try
            {
                os.Entity = new List<Message>(Entity
                    .Where(mes => mes.ConversationId == conversationId
                                && mes.UserId == userId));
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Messages received successfully";
            }
            catch (Exception e)
            {
                os.Entity = new List<Message>();
                os.StatusCode = DalOperationStatusCode.Error;
                os.Exception = e;
                os.Message = "Cannot receive messages";
            }
            return os;
        }

        public static DalOperationStatus<Message> GetMessage(int messageId)
        {
            DalOperationStatus<Message> os = new DalOperationStatus<Message>();
            try
            {
                os.Entity = Entity.Single(mes => mes.Id == messageId);
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Message received successfully";
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.StatusCode = DalOperationStatusCode.Error;
                os.Exception = e;
                os.Message = "Cannot receive messages";
            }
            return os;
        }
        
        #endregion

        #region Creation

        public static DalOperationStatus<Message> Create(int conversationId, int userId, string text = null)
        {
            DalOperationStatus<Message> os = new DalOperationStatus<Message>();
            try
            {
                User creator = UserRepository.Get(userId).Entity;

                Message message = new Message()
                {
                    Id = -1,
                    ConversationId = conversationId,
                    PostDateTime = DateTime.Now,
                    UserId = userId,
                };
                message.Text = (text != null) ? text.Replace('_', ' ') :
                  string.Format("{0} создал эту беседу {1} в {2}",
                    creator.Login, message.PostDateTime.ToShortDateString(), message.PostDateTime.ToShortTimeString());

                var creation = MessagesRepository.Create(message);
                os = creation;
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.Exception = e;
                os.Message = "Cannot create message";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public static DalOperationStatus<Message> Create(Message message)
        {
            DalOperationStatus<Message> os = new DalOperationStatus<Message>();
            try
            {
                if (message != null && message.Id == -1)
                {
                    message.Id = Entity.Count + 1;
                    Entity.Add(message);
                    os.Entity = message;
                    os.Message = "Message was created succeessfully";
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                }
                else
                {
                    os.Entity = null;
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Message is null or has invalid ID";
                }
            }
            catch (Exception e)
            {
                os.Entity = null;
                os.Exception = e;
                os.Message = "Cannot create message";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        #endregion
    }
}