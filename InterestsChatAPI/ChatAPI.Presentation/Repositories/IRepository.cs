﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAPI.Presentation.Repositories
{
    interface IRepository<T> : IDisposable
    {
        DalOperationStatus<List<T>> GetList();
        DalOperationStatus<T> Get(int id);
        DalOperationStatus<int> Create(T item);
        DalOperationStatus<int> Update(T item);
        DalOperationStatus<int> Delete(T item);
        DalOperationStatus<int> Delete(int itemId);
    }
}
