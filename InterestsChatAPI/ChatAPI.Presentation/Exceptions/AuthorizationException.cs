﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Exceptions
{
    public class AuthorizationException : Exception
    {
        public ExceptionReason Reason { get; protected set; }

        public AuthorizationException(ExceptionReason reason)
            : base(reason.ToString())
        {
            this.Reason = reason;
        }
    }
}