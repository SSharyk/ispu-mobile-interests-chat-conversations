﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Presentation.Exceptions
{
    public enum ExceptionReason
    {
        SuchUserEmailRegisteredAlready,
        InvalidEmailOrPassword
    }
}