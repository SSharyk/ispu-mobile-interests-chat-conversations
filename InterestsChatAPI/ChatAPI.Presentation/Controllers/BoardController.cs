﻿using ChatAPI.Presentation.Entities;
using ChatAPI.Presentation.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatAPI.Presentation.Controllers
{
    [RoutePrefix("api/board")]
    public class BoardController : BaseApiController
    {
        #region	Getting

        [HttpGet]
        [Route("{itemId}")]
        public BoardItem GetItem(int itemId)
        {
            var item = BoardRepository.Get(itemId);
            return (item.StatusCode == DalOperationStatusCode.OperationSuccessful) ? item.Entity : null;
        }

        [HttpGet]
        [Route("in/{conversationId}")]
        public List<BoardItem> GetItemsInConversation(int conversationId)
        {
            var item = BoardRepository.GetList(conversationId);
            return (item.StatusCode == DalOperationStatusCode.OperationSuccessful) ? item.Entity : null;
        }

        [HttpGet]
        [Route("ofUser/{userId}")]
        public List<BoardItem> GetItemsOfUser(int userId)
        {
            var item = BoardRepository.GetListOfUserNotes(userId);
            return (item.StatusCode == DalOperationStatusCode.OperationSuccessful) ? item.Entity : null;
        }

        #endregion

        #region CRUD

        [HttpGet]
        [Route("createnote/{conversationId}/{text}/{userId}")]
        public BoardItem CreateNote(int conversationId, string text, int userId)
        {
            var item = BoardRepository.CreateNote(conversationId, userId, text);
            return (item.StatusCode == DalOperationStatusCode.OperationSuccessful) ? item.Entity : null;
        }

        [HttpGet]
        [Route("updatenote/{noteId}/{text}")]
        public BoardItem UpdateNote(int noteId, string text)
        {
            var item = BoardRepository.UpdateNote(noteId, text);
            return (item.StatusCode == DalOperationStatusCode.OperationSuccessful) ? item.Entity : null;
        }

        [HttpGet]
        [Route("deletenote/{noteId}")]
        public int DeleteNote(int noteId)
        {
            var item = BoardRepository.DeleteNote(noteId);
            return (item.StatusCode == DalOperationStatusCode.OperationSuccessful) ? item.Entity : 0;
        }

        #endregion
    }
}