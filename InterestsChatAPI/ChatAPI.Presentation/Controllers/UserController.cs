﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ChatAPI.Presentation.Repositories;
using ChatAPI.Presentation.Models;
using ChatAPI.Presentation.Entities;
using System.Xml.Serialization;
using System.IO;

namespace ChatAPI.Presentation.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : BaseApiController
    {
        private UserRepository Repository;

        public UserController()
            : base()
        {
            Repository = new UserRepository(TheContext);
        }

        [HttpGet]
        [Route("{id}")]
        //[AuthenticationFilter(Roles = Roles.Administrator)]
        public UserModel GetUserById(int id)
        {
            var user = UserRepository.Get(id).Entity;
            return TheModelFactory.Create(user);
        }

        [Route("allusers")]
        [HttpGet]
        //[AuthenticationFilter(Roles = Roles.Administrator)]
        public List<UserModel> GetListOfUsers()
        {
            var list = UserRepository.GetList().Entity;
            return TheModelFactory.CreateListOfUsers(list).ToList();
        }

        [HttpGet]
        [Route("allusersnotfriends/{userId}")]
        public List<UserModel> GetNotFriends(int userId)
        {
            var list = FriendshipRepository.GetNotFriendsOfUser(userId).Entity;
            return TheModelFactory.CreateListOfUsers(list).ToList();
        }

        [Route("add/{id}")]
        [HttpGet]
        //[AuthenticationFilter(Roles = Roles.Administrator)]
        public int Add(int id)
        {
            return UserRepository.Create(new User() { Id = id, Email = "21222", Login = "22222", Password = "222", RoleId = 22, StatusId = -22 }).Entity;
        }

        [Route("friends/{userId}")]
        [HttpGet]
        public List<UserModel> GetFriends(int userId)
        {
            var users = FriendshipRepository.GetFriendsOfUser(userId).Entity;
            return TheModelFactory.CreateListOfUsers(users).ToList();
        }

        [Route("common/{first}/{second}")]
        [HttpGet]
        public int CommonConversationsCount(int first, int second)
        {
            return MembershipRepository.GetCommonConversationsCount(first, second).Entity;
        }
    }
}