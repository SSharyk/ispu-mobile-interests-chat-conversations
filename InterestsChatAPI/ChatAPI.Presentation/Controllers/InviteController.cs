﻿using ChatAPI.Presentation.Dictionaries;
using ChatAPI.Presentation.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatAPI.Presentation.Controllers
{
    [RoutePrefix("api/invite")]
    public class InviteController : BaseApiController
    {
        [HttpGet]
        [Route("unread/{userId}")]
        public List<Entities.Invite> GetListOfUserUnreadInvites(int userId)
        {
            var x = InviteRepository.GetList(userId, true);
            return x.Entity;
        }

        [HttpGet]
        [Route("sent/{userId}")]
        public List<Entities.Invite> GetListOfUserSentInvites(int userId)
        {
            var x = InviteRepository.GetList(userId, false);
            return x.Entity;
        }

        [HttpGet]
        [Route("friendship/{from}/{to}")]
        public int SendFriendshipInvite(int from, int to)
        {
            var x = InviteRepository.Send((new InviteReasonDictionary())["Friendship"], from, to);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful) ? x.Entity.Id : -1;
        }

        [HttpGet]
        [Route("membership/{from}/{to}/{conversationId}")]
        public int SendFriendshipInvite(int from, int to, int conversationId)
        {
            var x = InviteRepository.Send((new InviteReasonDictionary())["Membership"], from, to, conversationId);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful) ? x.Entity.Id : -1;
        }

        [HttpGet]
        [Route("accept/{inviteId}")]
        public bool AcceptInvite(int inviteId)
        {
            var x = InviteRepository.SetStatus(inviteId, (new InviteStatusDictionary())["Accepted"]);
            var invite = x.Entity;
            if (invite.EntityId != -1)
                MembershipRepository.Add(invite.ToUserId, invite.EntityId);
            else
                FriendshipRepository.Add(invite.FromUserId, invite.ToUserId);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful);
        }

        [HttpGet]
        [Route("cancel/{inviteId}")]
        public bool CancelInvite(int inviteId)
        {
            var x = InviteRepository.SetStatus(inviteId, (new InviteStatusDictionary())["Cancelled"]);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful);
        }
    }
}