﻿using ChatAPI.Presentation.Entities;
using ChatAPI.Presentation.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatAPI.Presentation.Controllers
{
    [RoutePrefix("api/message")]
    public class MessageController : ApiController
    {
        [HttpGet]
        [Route("{messageId}")]
        public Message GetMessage(int messageId)
        {
            var x = MessagesRepository.GetMessage(messageId);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful) ? x.Entity : null;
        }

        [HttpGet]
        [Route("in/{conversationId}")]
        public List<Message> GetMessages(int conversationId)
        {
            var x = MessagesRepository.GetConversationMessages(conversationId);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful) ? x.Entity : null;
        }

        [HttpPost]
        [Route("create")]
        public Message Create([FromBody] Message message)
        {
            var x = MessagesRepository.Create(message);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful) ? x.Entity : null;
        }

        [HttpGet]
        [Route("create/{conversationId}/{userId}/{text}")]
        public Message Create(int conversationId, int userId, string text)
        {
            var x = MessagesRepository.Create(conversationId, userId, text);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful) ? x.Entity : null;
        }
    }
}