﻿using ChatAPI.Presentation.Entities;
using ChatAPI.Presentation.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatAPI.Presentation.Controllers
{
    public class NotificationController : BaseApiController
    {
        [HttpGet]
        [Route("api/notification/{userId}")]
        public List<Notification> GetNotificationsForUser(int userId)
        {
            return NotificationRepository.GetList(userId).Entity;
        }
    }
}