﻿using ChatAPI.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatAPI.Presentation.Controllers
{
    public class BaseApiController : ApiController
    {
        private ModelFactory modelFactory;

        protected ModelFactory TheModelFactory
        {
            get
            {
                if (this.modelFactory == null)
                {
                    this.modelFactory = new ModelFactory();
                }
                return this.modelFactory;
            }
        }

        public ChatAPIContext TheContext { get; protected set; }

        public BaseApiController()
        {
            this.TheContext = new ChatAPIContext();
        }        
    }
}