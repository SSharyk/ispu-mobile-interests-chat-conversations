﻿using ChatAPI.Presentation.Entities;
using ChatAPI.Presentation.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatAPI.Presentation.Controllers
{
    [RoutePrefix("api/conversation")]
    public class ConversationController : ApiController
    {
        #region Groups

        [Route("categories")]
        [HttpGet]
        public List<string> GetCategories()
        {
            return ConversationRepository.GetCategoriesList().Entity;
        }

        #endregion

        #region Conversations

        [HttpGet]
        [Route("{id}")]
        public Conversation GetConversation(int id)
        {
            var x = ConversationRepository.Get(id);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful) ? x.Entity : null;
        }

        [HttpGet]
        [Route("ofUser/{userId}")]
        public List<Conversation> GetConversationsOfUser(int userId)
        {
            return MembershipRepository.GetConversationsOfUser(userId).Entity;
        }

        [HttpGet]
        [Route("byGroups/{userId}")]
        public Dictionary<int, List<Conversation>> GetConversationsOfUserByGroups(int userId)
        {
            return MembershipRepository.GetConversationsOfUser(userId).Entity
                .GroupBy(x => x.GroupId, x => x)
                .ToDictionary(x => x.Key, x => x.ToList());
        }

        [HttpGet]
        [Route("add/{userId}/{conversationId}")]
        public bool JoinConversation(int userId, int conversationId)
        {
            return MembershipRepository.Add(userId, conversationId).Entity;
        }

        [HttpPost]
        [Route("create")]
        public Conversation Create([FromBody] Conversation conversation)
        {
            var x = ConversationRepository.Create(conversation);
            return x.Entity;
        }

        [HttpGet]
        [Route("create/{name}/{privacyId}/{categoryId}/{creatorId}")]
        public Conversation Create(string name, int privacyId, int categoryId, int creatorId)
        {
            return ConversationRepository.Create(name, privacyId, categoryId, creatorId).Entity;
        }

        [HttpGet]
        [Route("publicForSelect/{userId}")]
        public List<Conversation> GetPublicConversations(int userId)
        {
            return ConversationRepository.GetListForSelection(userId).Entity;
        }

        [HttpGet]
        [Route("leave/{userId}/{conversationId}")]
        public bool Leave(int userId, int conversationId)
        {
            return MembershipRepository.Remove(userId, conversationId).Entity;
        }
        
        [HttpGet]
        [Route("delete/{userId}/{conversationId}")]
        public bool Delete(int userId, int conversationId)
        {
            return ConversationRepository.Remove(conversationId).Entity;
        }

        #endregion
    }
}