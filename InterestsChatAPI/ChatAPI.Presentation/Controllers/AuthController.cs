﻿using ChatAPI.Presentation.Entities;
using ChatAPI.Presentation.Models;
using ChatAPI.Presentation.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatAPI.Presentation.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : BaseApiController
    {
        [HttpGet]
        [Route("register/{email}/{login}/{password}")]
        public UserModel Register(string email, string login, string password)
        {
            var x = UserRepository.Create(email, login, password);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful) ? TheModelFactory.Create(x.Entity) : null;
        }

        [HttpGet]
        [Route("login/{email}/{password}")]
        public int Login(string email, string password)
        {
            var x = UserRepository.Get(email, password);
            return (x.StatusCode == DalOperationStatusCode.OperationSuccessful && x.Entity != null) ? x.Entity.Id : -1;
        }
    }
}