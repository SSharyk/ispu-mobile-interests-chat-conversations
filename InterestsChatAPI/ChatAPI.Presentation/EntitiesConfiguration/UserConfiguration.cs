﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ChatAPI.Presentation.Entities;

namespace ChatAPI.Presentation.EntitiesConfiguration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            this.ToTable("Users");

            this.HasKey(u => u.Id);
            this.Property(u => u.Id).IsRequired();
            this.Property(u => u.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(u => u.Login).IsRequired();
            this.Property(u => u.Login).HasMaxLength(16);

            this.Property(u => u.Email).IsRequired();
            this.Property(u => u.Email).HasMaxLength(255);

            this.Property(u => u.Password).IsRequired();
            this.Property(u => u.Password).IsFixedLength();
            this.Property(u => u.Password).HasMaxLength(32);

            //this.HasRequired(u => u.UserRole).WithMany(g => g.Users).HasForeignKey(u => u.RoleId);
            //this.HasRequired(u => u.UserStatus).WithMany(g => g.Users).HasForeignKey(u => u.StatusId);
        }
    }
}