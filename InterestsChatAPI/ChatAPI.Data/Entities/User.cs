﻿using System.Collections.Generic;
using ChatAPI.Data.Enums;

namespace ChatAPI.Data.Entities
{
    public class User
    {
        public int? Id { get; set; }

        public string Email { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public int RoleId { get; set; }
        public UserRole UserRole { get; set; }

        public int StatusId { get; set; }
        public UserStatus UserStatus { get; set; }
    }
}