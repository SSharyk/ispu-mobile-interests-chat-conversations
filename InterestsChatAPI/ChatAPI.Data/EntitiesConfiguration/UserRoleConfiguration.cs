﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ChatAPI.Data.Entities;


namespace ChatAPI.Data.EntitiesConfiguration
{
    class UserRoleConfiguration : EntityTypeConfiguration<UserRole>
    {
        public UserRoleConfiguration()
        {
            this.ToTable("UserRoles");

            this.HasKey(d => d.Id);
            this.Property(d => d.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(d => d.Id).IsRequired();

            this.Property(d => d.Name).IsRequired();
            this.Property(d => d.Name).HasMaxLength(127);

            this.HasMany(t => t.Users).WithRequired().HasForeignKey(d => d.RoleId);
        }
    }
}