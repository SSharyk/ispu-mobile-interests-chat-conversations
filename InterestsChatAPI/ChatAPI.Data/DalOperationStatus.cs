﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Data
{
    public class DalOperationStatus<T>
    {
        public DalOperationStatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public T Entity { get; set; }
        public int Count { get; set; } 

        public string BuildErrorMessageString()
        {
            string errorMessage = this.Message;
            if (this.Exception != null)
                errorMessage += ('\n' + this.Exception.Message);
            return errorMessage;
        }
    }

    public enum DalOperationStatusCode
    {
        Error,
        EntityNotFound,
        NoChanges,
        ChangesSaved,
        OperationSuccessful,
        UserIsDeletedError
    }
}