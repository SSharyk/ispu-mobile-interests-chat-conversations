﻿namespace ChatAPI.Data.Enums
{
    public enum UserStatusEnum
    {
        Online = 1,
        Offline = 2,
        TurnedOff = 3
    }
}