﻿namespace ChatAPI.Data.Enums
{
    public enum MembershipEnum
    {
        Creator = 1,
        Invited = 2,
        JoinedFree = 3
    }
}