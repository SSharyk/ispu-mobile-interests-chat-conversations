﻿namespace ChatAPI.Data.Enums
{
    public enum UserRoleEnum
    {
        User = 1,
        Admin = 2
    }
}