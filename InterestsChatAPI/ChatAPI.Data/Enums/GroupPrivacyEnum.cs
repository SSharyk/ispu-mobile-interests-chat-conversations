﻿namespace ChatAPI.Data.Enums
{
    public enum GroupPrivacyEnum
    {
        Public = 0,
        Private = 1
    }
}