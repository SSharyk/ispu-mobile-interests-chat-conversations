﻿using ChatAPI.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAPI.Data.Repositories.UserRepository
{
    public class UserRepository:IRepository<User>
    {
        private ChatAPIContext _context;

        public UserRepository(ChatAPIContext ctx)
        {
            this._context = ctx;
        }

        public DalOperationStatus<IQueryable<User>> GetList()
        {
            var os = new DalOperationStatus<IQueryable<User>>();
            try
            {
                os.Entity = _context.Users.AsQueryable();
                var usersCount = os.Entity.ToList<User>().Count;
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "List of users successfully received.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get users list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<User> Get(int? userId)
        {
            var os = new DalOperationStatus<User>();
            try
            {
                os.Entity = _context.Users.Where(us => us.Id == userId).Single();
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "List of users successfully received.";
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get user with the specific id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<int?> Create(User user)
        {
            var os = new DalOperationStatus<int?>();
            try
            {
                _context.Users.Add(user);
                if (_context.SaveChanges() > 0)
                {
                    os.Entity = user.Id;
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "User successfully added.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User not added.";
                }
            }
            catch (Exception ex)
            {
                //GlobalLogger.GetInstance().Error(ex, "User add error occured.");
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "User add error occured.";
                os.Exception = ex;
            }
            return os;
        }

        public DalOperationStatus<int?> Update(User user)
        {
            var os = new DalOperationStatus<int?>();
            var userOriginal = _context.Users.Find(user.Id);
            if (userOriginal != null)
            {
                try
                {
                    userOriginal.Login = user.Login;
                    userOriginal.Email = user.Email;
                    userOriginal.Password = user.Password;
                    userOriginal.RoleId = user.RoleId;
                    userOriginal.StatusId = user.StatusId;
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User wasn't updated.";
                    os.Exception = ex;
                    //GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a user.");
                    os.Entity = null;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "User not found.";
                os.Entity = null;
            }
            return os;
        }

        public DalOperationStatus<int?> Delete(User item)
        {
            throw new NotImplementedException();
        }

        public DalOperationStatus<int?> Delete(int? userId)
        {
            var os = new DalOperationStatus<int?>();
            var user = _context.Users.Find(userId);
            if (user != null)
            {
                try
                {
                    _context.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "User deleted.";
                    os.Entity = userId;
                }
                catch (Exception ex)
                {
                    //GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete an user.");
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User not deleted.";
                    os.Exception = ex;
                    os.Entity = null;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Entity = null;
                os.Message = "User not found.";
            }
            return os;
        }

        public void Dispose()
        {
        }
    }
}