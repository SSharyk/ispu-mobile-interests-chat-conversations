namespace ChatAPI.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ChatAPI.Data.ChatAPIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ChatAPI.Data.ChatAPIContext context)
        {
            new ChatAPIDataSeeder(context).Seed();
        }
    }
}
